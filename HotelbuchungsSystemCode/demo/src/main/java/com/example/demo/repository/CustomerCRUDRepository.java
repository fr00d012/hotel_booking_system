package com.example.demo.repository;

import com.example.demo.model.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Repository for Customer class
 */
@Repository
public interface CustomerCRUDRepository extends CrudRepository <Customer, Integer> {
   List<Customer> findByLastName(String lastName);
   List<Customer>findByFirstNameAndLastName(String firstName, String lastName);

   Optional<Customer>findByeMail(String eMail);


}
