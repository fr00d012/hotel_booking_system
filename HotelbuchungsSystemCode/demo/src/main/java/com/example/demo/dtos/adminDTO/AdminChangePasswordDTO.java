package com.example.demo.dtos.adminDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * DTO Class for change Admin password with old password
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AdminChangePasswordDTO {


    @NotEmpty(message = "Altes Passwort darf nicht leer sein.")
    @JsonProperty("oldPassword/SafeWord") private String oldPassword;

    @NotEmpty(message = "Bitte geben Sie ein neues Passwort ein.")
    @JsonProperty("newPassword") private String newPassword;

    @NotNull(message = "Mitarbeiter Id darf nicht leer sein. Bitte überprüfen Sie die Eingabe.")
    @JsonProperty("staffId") private Integer staffId;




}
