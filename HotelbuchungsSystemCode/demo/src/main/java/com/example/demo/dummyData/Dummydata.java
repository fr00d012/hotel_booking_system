package com.example.demo.dummyData;

import com.example.demo.enumclasses.Payment;
import com.example.demo.model.*;
import com.example.demo.repository.CustomerCRUDRepository;
import com.example.demo.repository.RoomCRUDRepository;
import com.example.demo.repository.StaffCRUDRepository;
import com.example.demo.repository.TransactionCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Class for all dummy datas
 */

@Component
public class Dummydata {


    @Autowired
    CustomerCRUDRepository customerCRUDRepository;
    @Autowired
    StaffCRUDRepository staffCRUDRepository;
    @Autowired
    RoomCRUDRepository roomCRUDRepository;
    @Autowired
    TransactionCRUDRepository transactionCRUDRepository;


    /**
     * Methode  too create a Random Staff (Firstname/Lastname/Streetname/Streetnumber/Zip Code/Country/Phonenumber/email/svnr/branche/rang/adminstatus)
     */
    public void createStaffRandom() {
        for (int i = 0; i < 15; i++) {
            String firstname = createRandomFirstname();
            String lastname = createRandomLastname();
            String email = createRandomEmail(firstname, lastname);
            boolean adminstatus = createRandomBoolean();
            String password = createRandomPassword();
            String saveWord = createRandomSaveWord();
            try {
                staffCRUDRepository.save(new Staff(firstname, lastname, email, adminstatus, password, saveWord));
            } catch (Exception e) {
                System.err.println("Fehler beim Einfügen des Datensatzes: " + e.getMessage());
            }
        }
        createStaffAdminstatusTrue();
        createStaffAdminstatusFalse();
    }

    /**
     * Methode to add one Dummy Staff with Admin Status true
     */
    public void createStaffAdminstatusTrue() {
        String firstname = createRandomFirstname();
        String lastname = createRandomLastname();
        String email = "coders.bay@gmx.at";
        boolean adminstatus = true;
        String password = createRandomPassword();
        String saveWord = createRandomSaveWord();
        try {
            staffCRUDRepository.save(new Staff(firstname, lastname, email, adminstatus, password, saveWord));
        } catch (Exception e) {
            System.err.println("Fehler beim Einfügen des Datensatzes: " + e.getMessage());
        }
    }

    public void createStaffAdminstatusFalse() {
        String firstname = createRandomFirstname();
        String lastname = createRandomLastname();
        String email = "coders.bay@yahoo.at";
        boolean adminstatus = false;
        String password = createRandomPassword();
        String saveWord = createRandomSaveWord();
        try {
            staffCRUDRepository.save(new Staff(firstname, lastname, email, adminstatus, password, saveWord));
        } catch (Exception e) {
            System.err.println("Fehler beim Einfügen des Datensatzes: " + e.getMessage());
        }
    }



    /**
     * Methode to add rooms to the transaction in the database
     */
    public void createRandomRoomForTransaction() {
        Random random = new Random();

        List<Transaction> transactions = (List<Transaction>) transactionCRUDRepository.findAll();
        for (Transaction transaction : transactions) {

            Set<Room> roomSet = new HashSet<>();
            int howMuchRooms = random.nextInt(4) + 1;
            for (int i = 1; i <= howMuchRooms; i++) {
                int roomrandom = random.nextInt(50) + 1;
                try {
                    Room room = roomCRUDRepository.findById(roomrandom).get();
                    roomSet.add(room);
                } catch (Exception e) {
                    System.err.println("Room zuweisung Transaction: " + e.getMessage());
                }
            }

            transaction.setRooms(roomSet);
            transaction.setFinalPrice(getFinalPriceDummyTransaction(transaction));
            transactionCRUDRepository.save(transaction);
        }
    }

    /**
     * Methode to get the correct price for the dummy Transacitons
     *
     * @param transaction created Transactions
     * @return Float correct price for the transcation
     */
    public Float getFinalPriceDummyTransaction(Transaction transaction) {

        long durationOfBooking;
        float pricePerNight = 0.0f;
        int durationInSaison = 0;

        Set<Room> rooms = transaction.getRooms();
        LocalDate saisonStart = LocalDate.of(transaction.getArriveDate().getYear(), 4, 30);
        LocalDate saisonEnd = LocalDate.of(transaction.getArriveDate().getYear(), 10, 1);


        for (Room room : rooms) {
            pricePerNight += room.getRoomPrice();
        }

        durationOfBooking = 1 + transaction.getArriveDate().until(transaction.getLeaveDate(), ChronoUnit.DAYS);

        if (transaction.getArriveDate().isBefore(saisonEnd) || transaction.getLeaveDate().isAfter(saisonStart)) {
            for (LocalDate checkDate = transaction.getArriveDate(); checkDate.isBefore(transaction.getLeaveDate()) || checkDate.isEqual(transaction.getLeaveDate()); checkDate = checkDate.plusDays(1)) {
                if (checkDate.isBefore(saisonStart) && checkDate.isBefore(saisonEnd)) {
                    durationInSaison++;
                }
            }
        }

        long durationOffSaison = durationOfBooking - durationInSaison;
        float priceOffSaison = durationOffSaison * pricePerNight;
        float priceInSaison = (durationInSaison * pricePerNight) * 1.1f;

        return priceInSaison + priceOffSaison;


    }

    /**
     * Methode too create a Random Customer(Firstname/Lastname/Birthdate/Streetname/Streetnumber/Country/City/Phonenumber/zipCode/email)
     */
    public void createCustomerRandom() {
        LocalDate date=createRandomDate();
        customerCRUDRepository.save(new Customer("Maria","Müller","Italien","Burggasse","12","45821","Rom","maria.mueller@gmx.de",date,"06605287458"));
        for (int i = 0; i < 20; i++) {
            String firstname = createRandomFirstname();
            String lastname = createRandomLastname();
            LocalDate birthdate = createRandomDate();
            String streetname = createRandomStreetName();
            String streetnumber = createRandomStreetNumber();
            String country = createRandomCountry();
            String city = createRandomCity(country);
            String phonenumber = createRandomPhoneNumber();
            String zipCode = createRandomZipCode();
            String eMail = createRandomEmail(firstname, lastname);
            try {
                customerCRUDRepository.save(new Customer(firstname, lastname, country, streetname, streetnumber, zipCode, city, eMail, birthdate, phonenumber));
            } catch (Exception e) {
                System.err.println("Fehler beim Einfügen des Datensatzes: " + e.getMessage());
            }
        }
    }

    /**
     * Methode too create Random Room(seaview/balcony/roomtype/roompricePerNight/NumberOfBeds )
     */
    public void createRoomRandom() {
        for (int i = 1; i < 51; i++) {
            Integer roomNumber = i;
            boolean seaview = createRandomBoolean();
            boolean balcony = createRandomBoolean();
            String roomtype = randomRoomtype();
            Integer numberOfBed = randomNumberOfBed(roomtype);
            float roompricePerNight = RoomPrice(roomtype, balcony, seaview);
            boolean bookable = true;
            try {
                roomCRUDRepository.save(new Room(roomNumber, roompricePerNight, numberOfBed, seaview, balcony, roomtype, bookable));
            } catch (Exception e) {
                System.err.println("Fehler beim Einfügen des Datensatzes: " + e.getMessage());
            }
        }
    }

    /**
     * Methode too create Random Transaction (leaveDate/arriveDate/finalPrice)
     */
    public void creatTransactionRandom() {
        for (int i = 0; i < 10; i++) {
            LocalDate arriveDate = createRandomArriveDate();
            LocalDate leaveDate = createRandomLeaveDate(arriveDate);
            float finalPrice = createFinalRandomPrice();
            Customer customer = createRandomCustomerId();
            LocalDate currentDate = LocalDate.now();
            Set<Headcount> headcounts = createHeadcoutnsSetForDummyTransaction();
            try {
                transactionCRUDRepository.save(new Transaction(arriveDate, leaveDate, Payment.CASH, currentDate, finalPrice, true, customer, headcounts));
            } catch (Exception e) {
                System.err.println("Fehler beim Einfügen des Datensatzes: " + e.getMessage());
            }
        }
    }

    /**
     * Methode to create random headcounts for transactions
     *
     * @return set with headcounts
     */
    public Set<Headcount> createHeadcoutnsSetForDummyTransaction() {
        Random random = new Random();
        Set<Headcount> headcounts = new HashSet<>();
        int headcountCounter = random.nextInt(3);
        for (int i = 0; i < headcountCounter; i++) {
            String firstname = createRandomFirstname();
            String lastname = createRandomLastname();
            LocalDate birthdate = createRandomDate();
            Headcount headcount = new Headcount(firstname, lastname, birthdate);
            headcounts.add(headcount);
        }
        return headcounts;
    }

    /**
     * Methode to get random customer to add to transaction
     *
     * @return customer
     */
    public Customer createRandomCustomerId() {
        Random randomnumber = new Random();
        int random = randomnumber.nextInt(20) + 1;
        Customer customers = new Customer();
        try {

            if(!(customerCRUDRepository.existsById(random))){
                random=1;
            }
            customers = customerCRUDRepository.findById(random).get();
        } catch (Exception e) {
            System.err.println("Fehler beim Hinzufügen Customer zu Transaction: " + e.getMessage());
        }
        return customers;
    }

    /**
     * Methode too create Random number/Price
     *
     * @return price
     */
    public float createFinalRandomPrice() {
        return (int) (Math.random() * 250);
    }

    /**
     * Methode too get a price for the room per night
     *
     * @param roomtype which roomtype
     * @param balcony  boolean
     * @param seaview  boolean
     * @return price per night
     */
    public float RoomPrice(String roomtype, boolean balcony, boolean seaview) {
        float price;
        switch (roomtype) {
            case "SINGLEBEDROOM":
                price = 45;
                if (balcony) {
                    price += 15;
                } else if (seaview) {
                    price += 10;
                }
                break;
            case "DOUBLEBEDROOM":
                price = 65;
                if (balcony) {
                    price += 15;
                } else if (seaview) {
                    price += 10;
                }
                break;
            case "DOUBLEBEDROOMSEPARATE":
                price = 70.00f;
                if (balcony) {
                    price += 15;
                } else if (seaview) {
                    price += 10;
                }
                break;
            case "SINGLEPLUSDOUBLEBEDROOM":
                price = 85;
                if (balcony) {
                    price += 15;
                } else if (seaview) {
                    price += 10;
                }
                break;
            case "PENTHOUSE":
                price = 250;
                if (balcony) {
                    price += 15;
                } else if (seaview) {
                    price += 10;
                }
                break;
            default:
                price = 0;
        }
        return price;
    }

    /**
     * Methode too create a random Roomtype
     *
     * @return roomtype
     */
    public String randomRoomtype() {
        int randomtype = (int) (Math.random() * 5);
        String roomtypes;
        switch (randomtype) {
            case 0:
                roomtypes = "SINGLEBEDROOM";
                break;
            case 1:
                roomtypes = "DOUBLEBEDROOM";
                break;
            case 2:
                roomtypes = "DOUBLEBEDROOMSEPARATE";
                break;
            case 3:
                roomtypes = "SINGLEPLUSDOUBLEBEDROOM";
                break;
            default:
                int random = (int) (Math.random() * 10);
                if (random < 2) {
                    roomtypes = "PENTHOUSE";
                } else {
                    roomtypes = "SINGLEBEDROOM";
                }
        }
        return roomtypes;

    }

    /**
     * Methode too get the number of bed from the room type
     *
     * @param roomtype String roomtype what u want
     * @return number of beds
     */
    public int randomNumberOfBed(String roomtype) {
        int numberofBed;
        if (roomtype.equals("SINGLEBEDROOM")) {
            numberofBed = 1;
        } else if (roomtype.equals("DOUBLEBEDROOM") || roomtype.equals("DOUBLEBEDROOMSEPARATE")) {
            numberofBed = 2;
        } else if (roomtype.equals("SINGLEPLUSDOUBLEBEDROOM")) {
            numberofBed = 3;
        } else if (roomtype.equals("PENTHOUSE")) {
            numberofBed = 5;
        } else {
            numberofBed = 0;
        }
        return numberofBed;
    }

    /**
     * Create random Boolen (used for adminstatus/balcony/seaview)
     *
     * @return boolean
     */
    public boolean createRandomBoolean() {
        int random = (int) (Math.random() * 10);
        if (random < 2) {
            return true;
        } else {
            return false;
        }
    }



    /**
     * Methode to create a random leave date for Transaction (not more than 30 day different between arrive and leave)
     *
     * @param arriveDate LocalDate arriveDate for checking
     * @return LocalDate leaveDate
     */
    public LocalDate createRandomLeaveDate(LocalDate arriveDate) {

        Random random = new Random();
        long addDays = random.nextInt(15) + 1;
        return arriveDate.plusDays(addDays);
    }

    /**
     * Methode too create a email with random ending
     *
     * @param firstname String
     * @param lastname  String
     * @return email
     */
    public String createRandomEmail(String firstname, String lastname) {
        String eMail = "";
        int randomEMail = (int) (Math.random() * 5);
        switch (randomEMail) {
            case 0:
                eMail = firstname.toLowerCase() + "." + lastname.toLowerCase() + "@gmx.at";
                break;
            case 1:
                eMail = firstname.toLowerCase() + "." + lastname.toLowerCase() + "@gamil.com";
                break;
            case 2:
                eMail = firstname.toLowerCase() + "." + lastname.toLowerCase() + "@yahoo.de";
                break;
            case 3:
                eMail = firstname.toLowerCase() + "." + lastname.toLowerCase() + "@outlook.com";
                break;
            case 4:
                eMail = firstname.toLowerCase() + "." + lastname.toLowerCase() + "@dogenhof.at";
                break;
        }
        return eMail;
    }

    /**
     * Methode too create a rando zipCode
     *
     * @return zipCode
     */
    public String createRandomZipCode() {
        String zipCode = "";
        int randomZipCode = (int) (Math.random() * 5);
        switch (randomZipCode) {
            case 0:
                zipCode = "7122";
                break;
            case 1:
                zipCode = "11201";
                break;
            case 2:
                zipCode = "10451";
                break;
            case 3:
                zipCode = "10301";
                break;
            case 4:
                zipCode = "10314";
                break;
        }
        return zipCode;
    }

    /**
     * Methode too create a random Phonenumber
     *
     * @return random Phonenumber
     */
    public String createRandomPhoneNumber() {
        String phonenumber = "";
        int randomPhoneNumber = (int) (Math.random() * 5);
        switch (randomPhoneNumber) {
            case 0:
                phonenumber = "+43/6602520745";
                break;
            case 1:
                phonenumber = "07227 20570";
                break;
            case 2:
                phonenumber = "+43/33352036";
                break;
            case 3:
                phonenumber = "+43/5173781320";
                break;
            case 4:
                phonenumber = "+43/6648324719";
                break;
        }
        return phonenumber;
    }

    /**
     * Methode too create a random Country
     *
     * @return random country
     */
    public String createRandomCountry() {
        String country = "";
        int randomCountry = (int) (Math.random() * 5);
        switch (randomCountry) {
            case 0:
                country = "Österreich";
                break;
            case 1:
                country = "Deutschland";
                break;
            case 2:
                country = "Arizone";
                break;
            case 3:
                country = "Kenia";
                break;
            case 4:
                country = "Frankreich";
                break;
        }
        return country;
    }

    /**
     * Methode too get a city what is in the create Country
     *
     * @param country which country
     * @return city
     */
    public String createRandomCity(String country) {
        String city = "";
        int randomCity = (int) (Math.random() * 5);
        if (country.equals("Österreich")) {
            switch (randomCity) {
                case 0:
                    city = "Wien";
                    break;
                case 1:
                    city = "Linz";
                    break;
                case 2:
                    city = "St.Pölten";
                    break;
                case 3:
                    city = "Styer";
                    break;
                case 4:
                    city = "Bregenz";
                    break;
            }
        } else if (country.equals("Deutschland")) {
            switch (randomCity) {
                case 0:
                    city = "Berlin";
                    break;
                case 1:
                    city = "Hamburg";
                    break;
                case 2:
                    city = "Düsseldorf";
                    break;
                case 3:
                    city = "Koblenz";
                    break;
                case 4:
                    city = "München";
                    break;
            }
        } else if (country.equals("Arizone")) {
            switch (randomCity) {
                case 0:
                    city = "Phoenix";
                    break;
                case 1:
                    city = "Tucson";
                    break;
                case 2:
                    city = "Scootsdale";
                    break;
                case 3:
                    city = "Sedona";
                    break;
                case 4:
                    city = "Flagstaff";
                    break;
            }
        } else if (country.equals("Kenia")) {
            switch (randomCity) {
                case 0:
                    city = "Nairobi";
                    break;
                case 1:
                    city = "Mobasa";
                    break;
                case 2:
                    city = "Nakuru";
                    break;
                case 3:
                    city = "Kisumu";
                    break;
                case 4:
                    city = "Kitale";
                    break;
            }
        } else if (country.equals("Frankreich")) {
            switch (randomCity) {
                case 0:
                    city = "Paris";
                    break;
                case 1:
                    city = "Marseille";
                    break;
                case 2:
                    city = "Bordeaux";
                    break;
                case 3:
                    city = "Lyon";
                    break;
                case 4:
                    city = "Nantes";
                    break;
            }
        }
        return city;
    }

    /**
     * Methode too create Random streetNumber
     *
     * @return street number
     */
    public String createRandomStreetNumber() {
        int random = (int) (Math.random() * 1000);
        return random + "";
    }

    /**
     * Methode too create random streetname
     *
     * @return streetname
     */
    public String createRandomStreetName() {
        String streetname = "";
        int randomNumber = (int) (Math.random() * 5);
        switch (randomNumber) {
            case 0:
                streetname = "Absberggasse";
                break;
            case 1:
                streetname = "Adolf-Kirchl-Gasse";
                break;
            case 2:
                streetname = "Alaudagasse";
                break;
            case 3:
                streetname = "Alpengasse";
                break;
            case 4:
                streetname = "Alte Laar Straße";
                break;
        }
        return streetname;
    }

    /**
     * Methode too create a random word
     *
     * @return saveWord
     */
    public String createRandomSaveWord() {
        String saveWord = "";
        int randomSaveWord = (int) (Math.random() * 11);
        switch (randomSaveWord) {
            case 0:
                saveWord = "Zitrone";
                break;
            case 1:
                saveWord = "Rosmarin";
                break;
            case 2:
                saveWord = "Thymian";
                break;
            case 3:
                saveWord = "Apfel";
                break;
            case 4:
                saveWord = "Birne";
                break;
            case 5:
                saveWord = "Kürbis";
                break;
            case 6:
                saveWord = "Tee";
                break;
            case 7:
                saveWord = "Kaffee";
                break;
            case 8:
                saveWord = "Cola";
                break;
            case 9:
                saveWord = "Fanta";
                break;
            case 10:
                saveWord = "Sprite";
                break;
        }
        return saveWord;
    }

    /**
     * Methode too create a random Password
     *
     * @return password
     */
    public String createRandomPassword() {
        String password = "";
        int randomPassword = (int) (Math.random() * 11);
        switch (randomPassword) {
            case 0:
                password = "123";
                break;
            case 1:
                password = "456";
                break;
            case 2:
                password = "789";
                break;
            case 3:
                password = "yxc";
                break;
            case 4:
                password = "asd";
                break;
            case 5:
                password = "qwe";
                break;
            case 6:
                password = "321";
                break;
            case 7:
                password = "654";
                break;
            case 8:
                password = "987";
                break;
            case 9:
                password = "zui";
                break;
            case 10:
                password = "hjk";
                break;
        }
        return password;
    }

    /**
     * Methode too create random date between 1940 and 2015
     *
     * @return random date
     */
    public LocalDate createRandomDate() {
        long minDay = LocalDate.of(1940, 1, 1).toEpochDay();
        long maxDay = LocalDate.of(2015, 12, 31).toEpochDay();
        long randomDay = ThreadLocalRandom.current().nextLong(minDay, maxDay);
        return LocalDate.ofEpochDay(randomDay);
    }

    /**
     * Methode to create a random arriveDate between 07/2022 and 12/2024
     *
     * @return random date(arriveDate)
     */
    public LocalDate createRandomArriveDate() {
        long minDay = LocalDate.of(2022, 8, 10).toEpochDay();
        long maxDay = LocalDate.of(2024, 12, 31).toEpochDay();
        long randomDay = ThreadLocalRandom.current().nextLong(minDay, maxDay);
        return LocalDate.ofEpochDay(randomDay);
    }

    /**
     * Methode too create random lastname
     *
     * @return lastname
     */
    public String createRandomLastname() {
        String lastname = "";
        int randomLastname = (int) (Math.random() * 11);
        switch (randomLastname) {
            case 0:
                lastname = "Schamberger";
                break;
            case 1:
                lastname = "Brunner";
                break;
            case 2:
                lastname = "Gruber";
                break;
            case 3:
                lastname = "Lunzer";
                break;
            case 4:
                lastname = "Carvinger";
                break;
            case 5:
                lastname = "Uzumaki";
                break;
            case 6:
                lastname = "Oshia";
                break;
            case 7:
                lastname = "Hiraya";
                break;
            case 8:
                lastname = "Oroshimaru";
                break;
            case 9:
                lastname = "Grimm";
                break;
            case 10:
                lastname = "Potter";
                break;
        }
        return lastname;
    }

    /**
     * Methode too create random firstname
     *
     * @return firstname
     */
    public String createRandomFirstname() {
        String firstname = "";
        int randomFirstname = (int) (Math.random() * 11);
        switch (randomFirstname) {
            case 0:
                firstname = "Misty";
                break;
            case 1:
                firstname = "Ash";
                break;
            case 2:
                firstname = "Rokky";
                break;
            case 3:
                firstname = "Jessi";
                break;
            case 4:
                firstname = "James";
                break;
            case 5:
                firstname = "Gerry";
                break;
            case 6:
                firstname = "Klaus";
                break;
            case 7:
                firstname = "Joko";
                break;
            case 8:
                firstname = "Freddi";
                break;
            case 9:
                firstname = "Jasmin";
                break;
            case 10:
                firstname = "Michael";
                break;
        }
        return firstname;
    }
}
