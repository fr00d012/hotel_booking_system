package com.example.demo.dtos.transactionDTO;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * extends transactionDTO to create a transaction per CustomerID(after login)
 */
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class TransactionDTOsWithId extends TransactionDTO {

    @JsonProperty("customerId")private Integer customerId;
}
