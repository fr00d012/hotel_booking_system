package com.example.demo.dtos.customerDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.time.LocalDate;

@Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class CustomerDTO {

    @JsonProperty("userId")private Integer userId;
    @JsonProperty("Firstname")private String firstname;
    @JsonProperty("Lastname")private String lastname;
    @JsonProperty("Country")private String country;
    @JsonProperty("Streetname")private String streetName;
    @JsonProperty("Streetnumber")private String streetNumber;
    @JsonProperty("Zipcode")private String zipCode;
    @JsonProperty("City")private String city;
    @JsonProperty("Birthdate")private LocalDate birthDate;
    @JsonProperty("Phonenumber")private String phoneNumber;
    @JsonProperty("E_mail")private String eMail;
}
