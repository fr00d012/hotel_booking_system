package com.example.demo.controller;

import com.example.demo.model.StornoTransaction;
import com.example.demo.service.StornoTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller Class Storno Transactions
 */
@RestController
public class StornoTransactionController {

    @Autowired
    StornoTransactionService stornoTransactionService;

    /**
     * Controller to get all StornoTransactions
     *
     * @return List with all StornoTransactions
     */
    @CrossOrigin
    @GetMapping("/stornoTransaction")
    public ResponseEntity<?> getAllStornoTransaction() {
        List<StornoTransaction> stornoTransactions = stornoTransactionService.getAStornoTransactionList();
        if (stornoTransactions.size() < 1) {
            return new ResponseEntity<>("Es gibt derzeit keine Stornierungen in der Datenbank.", HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(stornoTransactions, HttpStatus.OK);
        }
    }

    /**
     * Controller to get one stornoTransaction per Id
     *
     * @param stornoId id from the stornoTransaction
     * @return the stornotransaction
     */
    @CrossOrigin
    @GetMapping("/stornoTransaction/{stornoId}")
    public ResponseEntity<?> getOneStornoTransactionPerStornoId(@PathVariable Integer stornoId) {
        StornoTransaction stornoTransaction = stornoTransactionService.getOneStornoTransactionPerId(stornoId);
        if (stornoTransaction == null) {
            return new ResponseEntity<>("Es gibt keine Stornierung mit dieser Id. Bitte überprüfen Sie die Eingabe", HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(stornoTransaction, HttpStatus.OK);
        }
    }

    /**
     * Controller to delete one StornoTransaction per id
     *
     * @param stornoId id from the stornotransaction
     */
    @CrossOrigin
    @DeleteMapping("/delete/stornoTransaction/{stornoId}")
    public ResponseEntity<?> delteOneStornoTransactionPerStornoId(@PathVariable Integer stornoId) {
        String status = stornoTransactionService.deleteOneStornoTransactionPerId(stornoId);
        if (status.equals("Diese Stornierung wurde gelöscht.")) {
            return new ResponseEntity<>(status, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(status, HttpStatus.NOT_FOUND);
        }
    }
}
