package com.example.demo.dtos.transactionDTO;

import com.example.demo.dtos.transactionDTO.helperTransactionDTO.RoomDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class TransactionChangeRoomDTO {

    @JsonProperty("addRoomList") private List<RoomDTO> addRoomList;
    @JsonProperty("deleteRoomList")private List<RoomDTO>deleteRoomList;
    @JsonProperty("transactionId") private Integer transactionId;
}
