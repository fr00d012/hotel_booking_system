package com.example.demo.model;

import com.example.demo.enumclasses.Payment;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity for StornoTransactions
 */
@Setter @Getter @NoArgsConstructor @Entity @AllArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "stornoId")
public class StornoTransaction {

//--------------------------------VARIABLE------------------------------------------------------------------------------
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer stornoId;

    @Column(name="TransactionId", nullable = false)
    private Integer transactionId;

    @Column(name = "ArriveDate", nullable = false)
    private LocalDate arriveDate;

    @Column(name = "LeaveDate", nullable = false)
    private LocalDate leaveDate;

    @Enumerated(EnumType.STRING)
    private Payment payment;

    @Column(name="BookingDate", nullable = false)
    private LocalDate bookingDate;

    @Column(name = "FinalePrice", nullable = false)
    private float finalPrice;

    @Column(name="actually", nullable = false)
    private boolean actually;
//-------------------------------------CONNECTIONS----------------------------------------------------------------------

    @JsonIgnoreProperties({"stornoTransactions","transaction"})
    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name="customer_id")
    private Customer customerStorno;

    @JsonIgnoreProperties({"stornoTransactions","transaction"})
    @ManyToMany
    @JoinTable(
            name="StornoTabell",
            joinColumns = {@JoinColumn(name="stornoTransaction_id")},
            inverseJoinColumns = {@JoinColumn(name="roomNumberID")}
    )
    private Set<Room>roomsStorno=new HashSet<>();


//-----------------------------------------CONSTRUCTOR------------------------------------------------------------------




    public StornoTransaction(Integer transactionId, LocalDate arriveDate, LocalDate leaveDate, Payment payment, LocalDate bookingDate, float finalPrice, boolean actually) {
        this.transactionId = transactionId;
        this.arriveDate = arriveDate;
        this.leaveDate = leaveDate;
        this.payment = payment;
        this.bookingDate = bookingDate;
        this.finalPrice = finalPrice;
        this.actually = actually;
    }

}
