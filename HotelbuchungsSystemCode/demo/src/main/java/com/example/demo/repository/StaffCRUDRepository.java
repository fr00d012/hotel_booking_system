package com.example.demo.repository;

import com.example.demo.model.Staff;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import java.util.Optional;

/**
 * Repository for Staff
 */
@Repository
public interface StaffCRUDRepository extends CrudRepository <Staff, Integer> {

    Optional<Staff> findByeMail(String eMail);
}
