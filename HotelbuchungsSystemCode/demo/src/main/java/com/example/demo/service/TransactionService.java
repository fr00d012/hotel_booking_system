package com.example.demo.service;

import com.example.demo.dtos.billDTO.BillDTO;
import com.example.demo.dtos.billDTO.helperBillDTO.BillCustomerDTO;
import com.example.demo.dtos.billDTO.helperBillDTO.BillRoomDTO;
import com.example.demo.dtos.billDTO.helperBillDTO.BillTransactionDTO;
import com.example.demo.dtos.roomDTO.AllFreeRoomForADurationDTO;
import com.example.demo.dtos.roomDTO.RoomCategoryDTO;
import com.example.demo.dtos.transactionDTO.*;
import com.example.demo.dtos.transactionDTO.helperTransactionDTO.HeadcountDTO;
import com.example.demo.dtos.transactionDTO.helperTransactionDTO.RoomDTO;
import com.example.demo.dtos.updateDTO.ChangeHolidayDateDTO;
import com.example.demo.dtos.updateDTO.ChangeOneDateDTO;
import com.example.demo.exceptions.ExceptionStrings;
import com.example.demo.model.Customer;
import com.example.demo.model.Headcount;
import com.example.demo.model.Room;
import com.example.demo.model.Transaction;
import com.example.demo.repository.CustomerCRUDRepository;
import com.example.demo.repository.HeadcountCRUDRepository;
import com.example.demo.repository.RoomCRUDRepository;
import com.example.demo.repository.TransactionCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Service Class for Transaction
 */
@Service
public class TransactionService {


    @Autowired
    TransactionCRUDRepository transactionCRUDRepository;

    @Autowired
    CustomerCRUDRepository customerCRUDRepository;

    @Autowired
    RoomCRUDRepository roomCRUDRepository;

    @Autowired
    HeadcountCRUDRepository headcountCRUDRepository;

    @Autowired
    RoomService roomService;

    @Autowired
    StornoTransactionService stornoTransactionService;


//----------------------------------------------------GET METHODS-------------------------------------------------------

    /**
     * Methode to get all Transactions
     *
     * @return list of transaction
     */
    public List<Transaction> getAllTransaction() {
        return (List<Transaction>) transactionCRUDRepository.findAll();
    }

    /**
     * Methode to get one Transaction by Id
     *
     * @param transactionId Id of Transaction
     * @return Transaction
     */
    public Transaction getONeTransactionById(Integer transactionId) {
        if (!(transactionCRUDRepository.existsById(transactionId))) {
            return null;
        } else {
            return transactionCRUDRepository.findById(transactionId).get();
        }
    }


    /**
     * Methode to get all Transaction for a arrive Date
     *
     * @param arriveDate Date from the day u want to know how much gast/transactin are on the date
     * @return List of all Transaction for the date
     */
    public List<Transaction> getTransactionPerArriveDate(LocalDate arriveDate) {
        return transactionCRUDRepository.findByArriveDate(arriveDate);

    }

    /**
     * Methode to get the stornoPrice for a transaction
     *
     * @param transactionId id of the Transaction
     * @return pirce how much the customer must pay
     */
    public Float getStornoPriceFromATransactionPerId(Integer transactionId) {
        Transaction transaction = transactionCRUDRepository.findById(transactionId).get();
        return getThePaybackPrice(transaction);
    }

    /**
     * Methode to create a bill
     *
     * @param transactionId id of Transaction
     * @return BillDTO(Customer / HeadcounterSize / Transaction / Room)
     */
    public BillDTO getBillPerTransacitonId(Integer transactionId) {
        Transaction transaction = transactionCRUDRepository.findById(transactionId).get();
        List<BillRoomDTO> billRoomDTOS = new ArrayList<>();
        BillDTO billDTO = new BillDTO();


        Set<Headcount> headcounts = transaction.getHeadcount();
        Set<Room> rooms = transaction.getRooms();

        Customer customer = transaction.getCustomer();
        BillCustomerDTO billCustomerDTO = new BillCustomerDTO(customer.getFirstName(), customer.getLastName(), customer.getEMail());

        BillTransactionDTO billTransactionDTO = new BillTransactionDTO(transaction.getTransactionID(), transaction.getArriveDate(), transaction.getLeaveDate(), transaction.getPayment(), transaction.getFinalPrice());

        for (Room room : rooms) {
            BillRoomDTO billRoomDTO = new BillRoomDTO(room.getRoomNumber(), room.getRoomtype(), room.getRoomPrice());
            billRoomDTOS.add(billRoomDTO);
        }


        billDTO.setBillTransactionDTO(billTransactionDTO);
        billDTO.setBillRoomDTO(billRoomDTOS);
        billDTO.setBillCustomerDTO(billCustomerDTO);
        billDTO.setHeadcountsize(headcounts.size());
        return billDTO;
    }


//------------------------------------------------------POST METHODS----------------------------------------------------

    /**
     * add one Transaction too the Database
     *
     * @param transactionDTOsWithId transaction Datas ( ArriveDate/LeaveDate/Payment/FinalPrice)
     */
    public String createNewTransaction(TransactionDTOsWithId transactionDTOsWithId) {

        //Create  Variable the manage the booking
        Set<HeadcountDTO> headcountDTOS = transactionDTOsWithId.getHeadcount();
        Transaction transaction = transactionDTOsWithId.getTransaction();
        List<RoomDTO> roomIDS = transactionDTOsWithId.getRoomId();
        Set<Headcount> headcounts = new HashSet<>();

        boolean checkBooking;

        //checken ob die customerid in der datenbank vorhanden ist
        if (!(customerCRUDRepository.existsById(transactionDTOsWithId.getCustomerId()))) {
            return "Diese Gäste Id ist nicht vergeben. Bitte überprüfen Sie die Eingabe.";
        }

        Customer customer = customerCRUDRepository.findById(transactionDTOsWithId.getCustomerId()).get();

        StringBuilder status = new StringBuilder("Folgende Zimmer existieren nicht: ");
        int counter = 0;

        for (RoomDTO roomDTO : roomIDS) {
            if (!(roomCRUDRepository.existsById(roomDTO.getRoomId()))) {
                if (counter < 1) {
                    status.append(roomDTO.getRoomId());
                } else {
                    status.append(", ").append(roomDTO.getRoomId());
                }
                counter++;
            }
        }
        if (counter > 0) {
            return status + "";
        }

        //Checking if the Room is free
        checkBooking = checkFreeRoom(transactionDTOsWithId);
        if (!(checkBooking)) {
            return "Buchung nicht möglich weil \n" + checkWhichRoomIsNotFree(new TransactionDTO(transaction, transactionDTOsWithId.getHeadcount(), transactionDTOsWithId.getRoomId()));
        }
        //Checking if there are enough beds for the pax
        checkBooking = checkTheAssignment(transactionDTOsWithId);
        if (!(checkBooking)) {
            return "Buchung nicht möglich weil die gebuchten Zimmer nicht genug Betten haben.";
        }

        //Add all room object per id too the transaction
        for (RoomDTO roomDTO : roomIDS) {
            Room room = roomCRUDRepository.findById(roomDTO.getRoomId()).get();
            transaction.getRooms().add(room);
            room.getTransaction().add(transaction);
        }

        //save the Headcounts in the database

        for (HeadcountDTO headcount : headcountDTOS) {
            String firstname = headcount.getFirstname();
            String lastname = headcount.getLastname();
            LocalDate birthdate = headcount.getBirthdate();
            Headcount headcount1 = new Headcount(firstname, lastname, birthdate);
            headcount1.setTransaction(transaction);
            headcounts.add(headcount1);
        }


        //Get the final Price of the booking
        transaction.setBookingDate(LocalDate.now());
        float finalPrice = getFinalPriceOfTheBooking(transactionDTOsWithId);

        transaction.setActually(true);
        transaction.setFinalPrice(finalPrice);
        transaction.setHeadcount(headcounts);
        transaction.setCustomer(customer);
        transactionCRUDRepository.save(transaction);

        return "Buchung war Erfolgreich.";
    }

    /**
     * Methode to create New Transaction as Guest
     *
     * @param transactionDTOsAsGuest (Customer Data/Transaction Data/Headcounter Data)
     * @throws ExceptionStrings different ErrorCodes
     */
    public String createNewTransactionAsGuest(TransactionDTOsAsGuest transactionDTOsAsGuest) throws ExceptionStrings {

        Set<HeadcountDTO> headcountDTOS = transactionDTOsAsGuest.getHeadcount();
        Transaction transaction = transactionDTOsAsGuest.getTransaction();
        Customer customer = transactionDTOsAsGuest.getCustomer();
        List<RoomDTO> roomIDs = transactionDTOsAsGuest.getRoomId();
        Set<Headcount> headcounts = new HashSet<>();

        boolean checkingBooking;

        StringBuilder status = new StringBuilder("Folgende Zimmer existieren nicht: ");
        int counter = 0;

        for (RoomDTO roomDTO : roomIDs) {
            if (!(roomCRUDRepository.existsById(roomDTO.getRoomId()))) {
                if (counter < 1) {
                    status.append(roomDTO.getRoomId());
                } else {
                    status.append(", ").append(roomDTO.getRoomId());
                }
                counter++;
            }
        }
        if (counter > 0) {
            return status.toString();
        }

        checkingBooking = checkFreeRoom(transactionDTOsAsGuest);
        if (!(checkingBooking)) {
            return "Buchung nicht möglich weil\n" + checkWhichRoomIsNotFree(new TransactionDTO(transaction, transactionDTOsAsGuest.getHeadcount(), roomIDs));
        }

        checkingBooking = checkTheAssignment(transactionDTOsAsGuest);
        if (!(checkingBooking)) {
            return "Buchung nicht möglich weil die gebuchten Zimmer nicht genug Betten haben.";
        }

        //Save Customer into Database
        try {
            customerCRUDRepository.save(customer);
        } catch (Exception e) {
            throw new ExceptionStrings("Buchung nicht möglich bitte überprüfen Sie die Eingebenen Gäste Daten");
        }


        //Add all room object per id too the transaction
        for (RoomDTO roomDTO : roomIDs) {
            Room room = roomCRUDRepository.findById(roomDTO.getRoomId()).get();
            transaction.getRooms().add(room);
            room.getTransaction().add(transaction);
        }

        //save the Headcounts in the database
        for (HeadcountDTO headcount : headcountDTOS) {
            String firstname = headcount.getFirstname();
            String lastname = headcount.getLastname();
            LocalDate birthdate = headcount.getBirthdate();
            Headcount headcount1 = new Headcount(firstname, lastname, birthdate);
            headcount1.setTransaction(transaction);
            headcountCRUDRepository.save(headcount1);
            headcounts.add(headcount1);
        }

        //Get the final Price of the booking
        float finalPrice = getFinalPriceOfTheBooking(transactionDTOsAsGuest);

        transaction.setActually(true);
        transaction.setBookingDate(LocalDate.now());
        transaction.setFinalPrice(finalPrice);
        transaction.setHeadcount(headcounts);
        transaction.setCustomer(customer);
        transactionCRUDRepository.save(transaction);
        return "Buchung war Erfolgreich";
    }


//-----------------------------------------------------UPDATE METHODS---------------------------------------------------

    /**
     * Methode to change the days of the Booking arrive and leaveDate
     *
     * @param changeHolidayDateDTO (arriveDate/leaveDate/transactionId)
     * @return String with status message
     */
    public String updateOneTransactionArriveAndLeaveDate(ChangeHolidayDateDTO changeHolidayDateDTO) {

        Transaction transaction = transactionCRUDRepository.findById(changeHolidayDateDTO.getTransactionId()).get();
        Set<Room> newRoomSet = new HashSet<>();
        List<Room> roomList = roomService.getAllfreeRooms(new AllFreeRoomForADurationDTO(changeHolidayDateDTO.getArriveDate(), changeHolidayDateDTO.getLeaveDate()));
        long daysBetween = ChronoUnit.DAYS.between(LocalDate.now(), transaction.getArriveDate());

        if (daysBetween < 28) {
            return "Es tut uns sehr Leid aber leider können Sie die Anreise nicht verschieben weil Sie nur mehr 4 Wochen bis zum Anreisedatum haben.";
        }

        for (Room room : transaction.getRooms()) {
            for (Room room1 : roomList) {
                if (room.getRoomNumber().equals(room1.getRoomNumber())) {
                    newRoomSet.add(room);
                }
            }

        }
        if (!(newRoomSet.size() == transaction.getRooms().size())) {
            for (Room room : transaction.getRooms()) {

                if (!newRoomSet.contains(room)) {
                    List<Room> freeRoomCategory = roomService.getFreeRoomPerRoomCategory(new RoomCategoryDTO(changeHolidayDateDTO.getArriveDate(), changeHolidayDateDTO.getLeaveDate(), room.getRoomtype()));

                    if (freeRoomCategory.size() > 0) {
                        for(Room freeRoom:freeRoomCategory){
                            if(!newRoomSet.contains(freeRoom)){
                                newRoomSet.add(freeRoom);
                                break;
                            }
                }if(!(newRoomSet.size()==transaction.getRooms().size())){
                            return "Es konnten nicht alle Zimmmer ersetzt werden. Bitte wenden Sie sich telefonisch an unser Personal";
                    }
                    }
                    else {
                        return "Es konnte für das Zimmer " + room.getRoomNumber() + " mit der Zimmerkategorie " + room.getRoomtype() + " kein Ersatz gefunden werden.\n Bitte wendne Sie sich telefonisch an unser Mitarbeiter.";
                    }
                }
            }
        } else {
            newRoomSet = transaction.getRooms();
        }

        transaction.setArriveDate(changeHolidayDateDTO.getArriveDate());
        transaction.setLeaveDate(changeHolidayDateDTO.getLeaveDate());
        transaction.setRooms(newRoomSet);
        transaction.setFinalPrice((updateFinalPriceOfOneTransaction(transaction)) * 1.1f);

        transactionCRUDRepository.save(transaction);

        return "Buchung wurde aktualisiert.";

    }

    /**
     * Methode to update one transaction per transaction  id
     *
     * @param transaction   ( ArriveDate/LeaveDate/Payment/FinalPrice)
     * @param transactionid transaction id
     */
    public String updateOneTransactionById(Transaction transaction, Integer transactionid) {
        if (!(transactionCRUDRepository.existsById(transactionid))) {
            return "Buchung mit dieser BuchungsId existiert nicht.";
        }

        Transaction transactions = transactionCRUDRepository.findById(transactionid).get();
        transaction.setTransactionID(transaction.getTransactionID());
        transactions.setArriveDate(transaction.getArriveDate());
        transactions.setLeaveDate(transaction.getLeaveDate());
        transactions.setPayment(transaction.getPayment());
        transactions.setFinalPrice(transaction.getFinalPrice());
        transaction.setBookingDate(transaction.getBookingDate());
        transactionCRUDRepository.save(transactions);

        return "Buchung wurde geändert";
    }

    /**
     * Methode to update the Booking Date in the transaction
     *
     * @param newLocalDate  the date u want
     * @param transactionId transaction u want too change the booking date
     */
    public String updateOneTransactionByIdLocalDate(ChangeOneDateDTO newLocalDate, Integer transactionId) {
        if (!(transactionCRUDRepository.existsById(transactionId))) {
            return "Buchung mit dieser BuchungsId existiert nicht.";
        }
        Transaction transaction = transactionCRUDRepository.findById(transactionId).get();
        LocalDate newDate = newLocalDate.getLocalDate();
        transaction.setBookingDate(newDate);
        transactionCRUDRepository.save(transaction);
        return "BookingDate wurde aktualisiert.";
    }

    /**
     * Methode to change the room in one Transaction
     *
     * @param transactionChangeRoomDTO transaction id where the rooms changed
     * @return String status message
     */
    public String changeRoomsInTransaction(TransactionChangeRoomDTO transactionChangeRoomDTO) {

        if (!(transactionCRUDRepository.existsById(transactionChangeRoomDTO.getTransactionId()))) {
            return "Eine Buchung mit dieser Id existiert nicht.";
        }

        Transaction transaction = transactionCRUDRepository.findById(transactionChangeRoomDTO.getTransactionId()).get();
        Set<Room> rooms = transaction.getRooms();
        List<RoomDTO> deleteRoom = transactionChangeRoomDTO.getDeleteRoomList();
        List<RoomDTO> addedRoom = transactionChangeRoomDTO.getAddRoomList();
        List<Room> freeRoom = roomService.getAllfreeRooms(new AllFreeRoomForADurationDTO(transaction.getArriveDate(), transaction.getLeaveDate()));

        if (!(addedRoom.isEmpty())) {
            for (RoomDTO checkRoom : addedRoom) {
                Room room = roomCRUDRepository.findById(checkRoom.getRoomId()).orElse(null);

                if (room == null) {
                    return "Dieses Zimmer mit der Zimmernummer " + checkRoom.getRoomId() + " exsistiert nicht";
                }
                else if (rooms.contains(room)) {
                    return "Das Zimmer mit der Zimmernummer " + room.getRoomNumber() + " ist in der Buchung schon vorhanden. \nÄnderung wurde nicht durchgeführt. \nBitte versuchen Sie es erneut.";
                }
               else if (!(freeRoom.contains(room))) {
                    return "Folgendes Zimmer ist  leider nicht frei für diesen Zeitraum: " + room.getRoomNumber();
                }
                rooms.add(room);
            }
        }

        if (!(deleteRoom.isEmpty())) {
            for (RoomDTO roomDTO : deleteRoom) {
                int i = 0;
                for (Room room : transaction.getRooms()) {

                    if (room.getRoomNumber().equals(roomDTO.getRoomId())) {
                        rooms.remove(room);
                        i++;
                        break;
                    }

                }
                if (i == 0) {
                    return "Das Zimmer mit der Zimmernummer " + roomDTO.getRoomId() + " ist nicht in der Buchung vorhanden";
                }
            }
        }


        float oldPrice = transaction.getFinalPrice();
        transaction.setRooms(rooms);
        float newPrice = updateFinalPriceOfOneTransaction(transaction);
        if (oldPrice > newPrice) {
            transaction.setFinalPrice(newPrice);
        }
        transactionCRUDRepository.save(transaction);

        return "Änderung der Zimmer erfolgreich.";
    }
//----------------------------------------------DELETE METHODS----------------------------------------------------------

    /**
     * Delete one Transaction by Id
     *
     * @param transactionId trnasactionId
     */
    public String deleteOneTransaction(Integer transactionId) {
        if (!(transactionCRUDRepository.existsById(transactionId))) {
            return "Diese BuchungsId ist nicht vergeben";
        }
        transactionCRUDRepository.deleteById(transactionId);
        return "Buchung gelöscht";
    }

    /**
     * create a Storno Transaction (delete the transaction from the database and add it to the stornoTransaction Database)
     *
     * @param transactionId transaction id who gets storno
     * @return bill with the stornoCosts
     */
    public BillDTO createOneStronoPerTransactionId(Integer transactionId) {

        Transaction transaction = transactionCRUDRepository.findById(transactionId).get();
        BillDTO billDTO;

        if (transaction.isActually()) {

            transaction.setActually(false);
            stornoTransactionService.addStornoTransactionInDatabase(transaction);

            transaction.setFinalPrice(transaction.getFinalPrice()-getThePaybackPrice(transaction));
            billDTO = getBillPerTransacitonId(transactionId);
            transactionCRUDRepository.deleteById(transactionId);
        } else {
            return null;
        }
        return billDTO;
    }

//-----------------------------------------------HELPER METHODS--------------------------------------------------------


    /**
     * Methode to get the day how long the booking is going
     *
     * @param arriveDate start date from the duration
     * @param leaveDate  end date from the duration
     * @return length of the peroid
     */
    public Long TheDurationOfBooking(LocalDate arriveDate, LocalDate leaveDate) {
        long durationOfHoliday = arriveDate.until(leaveDate, ChronoUnit.DAYS);
        if (durationOfHoliday == 0) {
            return durationOfHoliday = 1;
        }
        return durationOfHoliday;
    }

    /**
     * Methode to get the full price of the booking
     *
     * @param transactionDTO data from the customer(arrive/leave/roomnumber/customerid)
     * @return final price
     */
    public Float getFinalPriceOfTheBooking(TransactionDTO transactionDTO) {

        List<RoomDTO> roomIDs = transactionDTO.getRoomId();
        Transaction transaction = transactionDTO.getTransaction();

        long durationOfBooking = TheDurationOfBooking(transaction.getArriveDate(), transaction.getLeaveDate());
        int yearOfTheBooking = transaction.getArriveDate().getYear();
        float finalprice = 0.0f;
        long durationInSaison = 0;


        LocalDate saisonStart = LocalDate.of(yearOfTheBooking, 5, 30);
        LocalDate saisonEnd = LocalDate.of(yearOfTheBooking, 9, 1);


        for (RoomDTO roomDTO : roomIDs) {
            Room rooms = roomCRUDRepository.findById(roomDTO.getRoomId()).get();
            finalprice += rooms.getRoomPrice();
        }

        if (transaction.getArriveDate().isBefore(saisonEnd) || transaction.getLeaveDate().isAfter(saisonStart)) {

            for (LocalDate checkDate = transaction.getArriveDate();
                 checkDate.isBefore(transaction.getLeaveDate()) || checkDate.isEqual(transaction.getLeaveDate());
                 checkDate = checkDate.plusDays(1)) {

                if (checkDate.isAfter(saisonStart) && checkDate.isBefore(saisonEnd)) {
                    ++durationInSaison;
                }
            }
        }


        long durationOffSaison = durationOfBooking - durationInSaison;
        float priceOffSaison = durationOffSaison * finalprice;
        float priceInSaison = (durationInSaison * finalprice) * 1.1f;

        finalprice = priceInSaison + priceOffSaison;

        return finalprice;
    }

    /**
     * Methode to get the pre price from a Booking
     *
     * @param prePriceTransactionDTO (Transaction Object Data/Room Numbers)
     * @return float with the price for this booking
     */
    public Float getPrePriceOfTheBooking(PrePriceTransactionDTO prePriceTransactionDTO) {
        List<RoomDTO> roomIDs = prePriceTransactionDTO.getRoomId();
        Transaction transaction = prePriceTransactionDTO.getTransaction();
        long durationOfBooking = TheDurationOfBooking(transaction.getArriveDate(), transaction.getLeaveDate());
        int yearOfTheBooking = transaction.getArriveDate().getYear();
        LocalDate saisonStart = LocalDate.of(yearOfTheBooking, 5, 30);
        LocalDate saisonEnd = LocalDate.of(yearOfTheBooking, 9, 1);
        float finalprice = 0.0f;
        long durationInSaison = 0;

        for (RoomDTO roomDTO : roomIDs) {
            Room rooms = roomCRUDRepository.findById(roomDTO.getRoomId()).get();
            finalprice += rooms.getRoomPrice();
        }

        if (transaction.getArriveDate().isBefore(saisonEnd) || transaction.getLeaveDate().isAfter(saisonStart)) {
            for (LocalDate checkDate = transaction.getArriveDate(); checkDate.isBefore(transaction.getLeaveDate()) || checkDate.isEqual(transaction.getLeaveDate()); checkDate = checkDate.plusDays(1)) {
                if (checkDate.isAfter(saisonStart) && checkDate.isBefore(saisonEnd)) {
                    ++durationInSaison;
                }
            }
        }


        long durationOffSaison = durationOfBooking - durationInSaison;
        float priceOffSaison = durationOffSaison * finalprice;
        float priceInSaison = (durationInSaison * finalprice) * 1.1f;

        finalprice = priceInSaison + priceOffSaison;

        return finalprice;
    }

    /**
     * Methode to update the final price in a transaction after change the rooms
     *
     * @param transaction (Transaction Data)
     * @return the new final price for this transaction
     */
    public Float updateFinalPriceOfOneTransaction(Transaction transaction) {
        long durationOfBooking = TheDurationOfBooking(transaction.getArriveDate(), transaction.getLeaveDate());
        int yearOfTheBooking = transaction.getArriveDate().getYear();
        LocalDate saisonStart = LocalDate.of(yearOfTheBooking, 5, 30);
        LocalDate saisonEnd = LocalDate.of(yearOfTheBooking, 9, 1);
        float finalprice = 0.0f;
        long durationInSaison = 0;

        for (Room room : transaction.getRooms()) {
            finalprice += room.getRoomPrice();
        }

        if (transaction.getArriveDate().isBefore(saisonEnd) || transaction.getLeaveDate().isAfter(saisonStart)) {
            for (LocalDate checkDate = transaction.getArriveDate(); checkDate.isBefore(transaction.getLeaveDate()) || checkDate.isEqual(transaction.getLeaveDate()); checkDate = checkDate.plusDays(1)) {
                if (checkDate.isAfter(saisonStart) && checkDate.isBefore(saisonEnd)) {
                    ++durationInSaison;
                }
            }
        }


        long duratonOffSaison = durationOfBooking - durationInSaison;
        float priceOffSaison = duratonOffSaison * finalprice;
        float priceInSaison = (durationInSaison * finalprice) * 1.1f;

        finalprice = priceInSaison + priceOffSaison;

        return finalprice;
    }

    /**
     * Methode to check if the number of guest are not more than the number of beds in the rooms
     *
     * @param transactionDTO data from the customer(arrive/leave/roomnumber/customerid)
     * @return boolean true (number of beds>=guest) false(number of beds<guest)
     */
    public boolean checkTheAssignment(TransactionDTO transactionDTO) {

        Set<HeadcountDTO> headcounts = transactionDTO.getHeadcount();
        List<RoomDTO> rooms = transactionDTO.getRoomId();
        int countHeadcount = 1 + headcounts.size();
        int countNumberOfBed = 0;
        for (RoomDTO roomDTO : rooms) {
            Room room = roomCRUDRepository.findById(roomDTO.getRoomId()).get();
            countNumberOfBed += room.getNumberOfBed();
        }
        return countHeadcount <= countNumberOfBed;

    }

    /**
     * Methode to check if the room is free for the specific date
     *
     * @param transactionDTO data from the customer(transaction/headcounter/roomnumber)
     * @return boolean (true=room is free/ false=room is not free)
     */
    public boolean checkFreeRoom(TransactionDTO transactionDTO) {
        Transaction transaction = transactionDTO.getTransaction();
        AllFreeRoomForADurationDTO allFreeRoomForADurationDTO = new AllFreeRoomForADurationDTO(transaction.getArriveDate(), transaction.getLeaveDate());
        List<RoomDTO> roomDTOS = transactionDTO.getRoomId();
        List<Room> freeRoom = new ArrayList<>();
        boolean roomIsFree = true;

        List<Room> freeRoomList = roomService.checkAllFreeRoomsToCreateTransaction(allFreeRoomForADurationDTO);

        for (RoomDTO roomDTO : roomDTOS) {
            for (Room room : freeRoomList) {
                if (room.getRoomNumber().equals(roomDTO.getRoomId())) {
                    freeRoom.add(room);
                }
            }
        }
        if (freeRoom.size() != roomDTOS.size()) {
            roomIsFree = false;
        }

        return roomIsFree;
    }

    /**
     * Methode to get the Strono price if the customer want to storno
     *
     * @param transaction id of the transaction what the customer want to storno
     * @return new Price after storno
     */
    public Float getThePaybackPrice(Transaction transaction) {

        LocalDate currentDate = LocalDate.now();
        LocalDate transactionBookingDate = transaction.getBookingDate();

        float stornoPrice;
        float finalPrice = transaction.getFinalPrice();
        long timeBetweenArrive = transactionBookingDate.until(transaction.getArriveDate(), ChronoUnit.DAYS);
        long timeBetweenBooking = currentDate.until(transaction.getBookingDate(), ChronoUnit.DAYS);

        if (timeBetweenBooking <= 7 && timeBetweenBooking >= 0) {
            stornoPrice = finalPrice;
        } else {
            if (timeBetweenArrive <= 7) {
                stornoPrice = 0;
            } else if (timeBetweenArrive <= 14) {
                stornoPrice = finalPrice * 0.3f;
            } else if (timeBetweenArrive <= 21) {
                stornoPrice = finalPrice * 0.5f;
            } else {
                stornoPrice = finalPrice * 0.8f;
            }
        }
        return stornoPrice;
    }

    /**
     * Methode to get list with all transcaiton per customer id (helper methode for delete customer methode)
     *
     * @param customerId id from the customer
     * @return list with all transcation
     */
    public List<Transaction> getAllTransacitonPerCustomerId(Integer customerId) {
        return transactionCRUDRepository.findByCustomerUserID(customerId);
    }


    /**
     * Methode to get all roomnumbers who are not free
     *
     * @param transactionDTO (Transaction Data/Headcounter set/Room ids)
     * @return StringBuilder with all roomNumber who are not free.
     */
    public StringBuilder checkWhichRoomIsNotFree(TransactionDTO transactionDTO) {

        StringBuilder status = new StringBuilder("Folgende Zimmernummer sind leider schon vergeben: ");

        List<Room> freeRoom = roomService.getAllfreeRooms(new AllFreeRoomForADurationDTO(transactionDTO.getTransaction().getArriveDate(), transactionDTO.getTransaction().getLeaveDate()));

        List<RoomDTO> checklist = new ArrayList<>(transactionDTO.getRoomId());

        for (RoomDTO roomDTO : transactionDTO.getRoomId()) {

            for (Room room : freeRoom) {
                if (roomDTO.getRoomId().equals(room.getRoomNumber())) {
                    checklist.remove(roomDTO);
                }
            }
        }

        int i = 0;
        for (RoomDTO roomDTO : checklist) {

            if (i < 1) {
                status.append(roomDTO.getRoomId());
            } else {
                status.append(", ").append(roomDTO.getRoomId());
            }
            i++;
        }
        return status;
    }

    /**
     * Methode to check if the rooms are existing
     *
     * @param prePriceTransactionDTO TransactionData(arriveDate/leaveDate/payment/finalprice(empty or null) and List with all room Numbers
     * @return list with rooms who are not exsiting
     */
    public List<RoomDTO> checkIfRoomExisting(PrePriceTransactionDTO prePriceTransactionDTO) {
        List<RoomDTO> roomDTOS = prePriceTransactionDTO.getRoomId();
        List<RoomDTO> whichRoomNotExisting = new ArrayList<>();

        for (RoomDTO roomDTO : roomDTOS) {
            if (!(roomCRUDRepository.existsById(roomDTO.getRoomId()))) {
                whichRoomNotExisting.add(roomDTO);
            }
        }

        return whichRoomNotExisting;
    }


}
