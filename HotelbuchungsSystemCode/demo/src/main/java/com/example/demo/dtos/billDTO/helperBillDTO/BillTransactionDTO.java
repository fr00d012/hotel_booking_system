package com.example.demo.dtos.billDTO.helperBillDTO;

import com.example.demo.enumclasses.Payment;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

/**
 * Helper DTO for the Bill (Transaction)
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BillTransactionDTO {
    @JsonProperty("transactionId") private Integer transactionId;
    @JsonProperty("arriveDate") private LocalDate arriveDate;
    @JsonProperty("leaveDate")private LocalDate leaveDate;
    @JsonProperty("payment")private Payment payment;
    @JsonProperty("finalPrice")private Float finalprice;
}
