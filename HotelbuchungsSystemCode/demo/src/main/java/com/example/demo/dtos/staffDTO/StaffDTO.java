package com.example.demo.dtos.staffDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter @NoArgsConstructor @AllArgsConstructor @Setter
public class StaffDTO {

    @NotEmpty(message = "Bitte geben Sie einen Vornamen ein.")
    @JsonProperty("firstName")private String firstName;

    @NotEmpty(message = "Bitte geben Sie einen Nachnamen ein.")
    @JsonProperty("lastName")private String lastName;


    @JsonProperty("email")private String eMail;

    @NotNull(message = "Bitte geben Sie den Adminstatus an.")
    @JsonProperty("adminstatus")private boolean adminstatus;
}
