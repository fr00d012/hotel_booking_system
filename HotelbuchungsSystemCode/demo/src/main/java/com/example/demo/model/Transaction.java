package com.example.demo.model;

import com.example.demo.enumclasses.Payment;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity for Transacitons
 */
@Setter @Getter @NoArgsConstructor @Entity @AllArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "transactionID")
public class Transaction {
    //-------------------VARIABLE------------------------------------------------
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer transactionID;

    @Column(name = "ArriveDate", nullable = false)
    @NotNull(message = "Bitte geben Sie ein Anreisedatum an.")
    @FutureOrPresent(message = "Datum ist in der Vergangenheit.")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate arriveDate;

    @Column(name = "LeaveDate", nullable = false)
    @NotNull(message = "Bitte geben Sie ein Abreisedatum an.")
    @JsonFormat(pattern="yyyy-MM-dd")
    @FutureOrPresent(message = "Datum ist in der Vergangenheit.")
    private LocalDate leaveDate;

    @Enumerated(EnumType.STRING)
    @NotNull(message = "Bitte geben Sie eine Zahlungsart ein.")
    private Payment payment;

    @Column(name="BookingDate", nullable = false)
    private LocalDate bookingDate;

    @Column(name = "FinalePrice", nullable = false)
    private float finalPrice;

    @Column(name="actually", nullable = false)
    private boolean actually;
    //-------------------------CONNECTIONS-------------------------------

    @JsonIgnoreProperties("transaction")
    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "customner_id")
    private Customer customer;

    @JsonIgnoreProperties("transaction")
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "Guestlist",
            joinColumns = {@JoinColumn(name = "transact_id")},
            inverseJoinColumns = {@JoinColumn(name = "headcounterId")}
    )
    private Set<Headcount> headcount = new HashSet<>();

    @JsonIgnoreProperties("transaction")
    @ManyToMany(cascade = CascadeType.DETACH)
    @JoinTable(
            name = "Bookinglist",
            joinColumns = {@JoinColumn(name = "transaction_id")},
            inverseJoinColumns = {@JoinColumn(name = "roomnumber_id")}
    )
    private Set<Room> rooms = new HashSet<>();



    //-----------------------CONSTRUCTOR--------------------------------------

    public Transaction(LocalDate arriveDate, LocalDate leaveDate, Payment payment, LocalDate bookingDate, float finalPrice, Customer customer) {
        this.arriveDate = arriveDate;
        this.leaveDate = leaveDate;
        this.payment = payment;
        this.bookingDate = bookingDate;
        this.finalPrice = finalPrice;
        this.customer = customer;
    }

    public Transaction(LocalDate arriveDate, LocalDate leaveDate, Payment payment, float finalPrice, Customer customer, Set<Headcount> headcount) {
        this.arriveDate = arriveDate;
        this.leaveDate = leaveDate;
        this.payment = payment;
        this.finalPrice = finalPrice;
        this.customer = customer;
        this.headcount = headcount;
    }

    public Transaction(LocalDate arriveDate, LocalDate leaveDate, Payment payment, LocalDate bookingDate, float finalPrice, boolean actually, Customer customer, Set<Headcount> headcount) {
        this.arriveDate = arriveDate;
        this.leaveDate = leaveDate;
        this.payment = payment;
        this.bookingDate = bookingDate;
        this.finalPrice = finalPrice;
        this.actually = actually;
        this.customer = customer;
        this.headcount = headcount;
    }

    public Transaction(LocalDate arriveDate, LocalDate leaveDate, Payment payment, LocalDate bookingDate, float finalPrice, boolean actually, Customer customer, Set<Headcount> headcount, Set<Room> rooms) {
        this.arriveDate = arriveDate;
        this.leaveDate = leaveDate;
        this.payment = payment;
        this.bookingDate = bookingDate;
        this.finalPrice = finalPrice;
        this.actually = actually;
        this.customer = customer;
        this.headcount = headcount;
        this.rooms = rooms;
    }

    //---------------------------------------GETTER & SETTER-----------------------------------------


}
