package com.example.demo.dtos.billDTO.helperBillDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Helper DTO for the Bill (Room)
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BillRoomDTO {

    @JsonProperty("RoomNr") private Integer roomNumber;
    @JsonProperty("RoomTyp") private String roomtype;
    @JsonProperty("RoomPrice") private Float roomprice;
}
