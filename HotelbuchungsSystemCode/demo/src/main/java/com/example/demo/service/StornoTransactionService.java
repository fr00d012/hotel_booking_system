package com.example.demo.service;


import com.example.demo.model.Customer;
import com.example.demo.model.Room;
import com.example.demo.model.StornoTransaction;
import com.example.demo.model.Transaction;
import com.example.demo.repository.RoomCRUDRepository;
import com.example.demo.repository.StornoTransactionCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Service Class for StornoTransaction
 */
@Service
public class StornoTransactionService {

    @Autowired
    StornoTransactionCRUDRepository stornoTransactionCRUDRepository;

    @Autowired
    RoomCRUDRepository roomCRUDRepository;

//-----------------------------------------GET METHODS------------------------------------------------------------------

    /**
     * Methode to get all stornoTransaction
     *
     * @return List with all stornoTransaction
     */
    public List<StornoTransaction> getAStornoTransactionList() {
        return (List<StornoTransaction>) stornoTransactionCRUDRepository.findAll();

    }

    /**
     * Methode to get one stornoTransaction per id
     *
     * @param stornoId id from the stornotransaction
     * @return the wanted stornoTransaction
     */
    public StornoTransaction getOneStornoTransactionPerId(Integer stornoId) {
        if (!(stornoTransactionCRUDRepository.existsById(stornoId))) {
            return null;
        } else {
            return stornoTransactionCRUDRepository.findById(stornoId).get();
        }
    }
//-----------------------------------------DELETE METHODS---------------------------------------------------------------

    /**
     * Methode to delete one storno transaction per id
     * @param stornoId id from the storno who should get deleted
     * @return String with status message
     */
    public String deleteOneStornoTransactionPerId(Integer stornoId) {
        if (!(stornoTransactionCRUDRepository.existsById(stornoId))) {
            return "Diese Id existiert nicht. Bitte überprüfen Sie die Eingabe";
        } else {
            stornoTransactionCRUDRepository.deleteById(stornoId);
            return "Diese Stornierung wurde gelöscht.";
        }
    }

//-----------------------------------------HELPER METHODS---------------------------------------------------------------

    /**
     * Helper Methode to transver the transaction from the transaction Database to the stornoDatabase
     *
     * @param transaction transaciton what get stornoed
     */
    public void addStornoTransactionInDatabase(Transaction transaction) {

        Customer customer = transaction.getCustomer();
        Set<Room> roomSet = new HashSet<>(transaction.getRooms());

        StornoTransaction stornoTransaction = new StornoTransaction(transaction.getTransactionID(),
                transaction.getArriveDate(),
                transaction.getLeaveDate(),
                transaction.getPayment(),
                transaction.getBookingDate(),
                transaction.getFinalPrice(),
                transaction.isActually());

        stornoTransaction.setCustomerStorno(customer);

        stornoTransaction.setRoomsStorno(roomSet);
        for (Room room : roomSet) {
            room.setStornoTransactions(room.getStornoTransactions());
            roomCRUDRepository.save(room);
        }

        stornoTransactionCRUDRepository.save(stornoTransaction);
    }
}
