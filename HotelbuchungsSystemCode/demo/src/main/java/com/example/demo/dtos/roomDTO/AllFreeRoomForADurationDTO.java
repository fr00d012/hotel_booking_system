package com.example.demo.dtos.roomDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.time.LocalDate;

/**
 * DTO for two Dates (arriveDate/leaveDate)
 */
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class AllFreeRoomForADurationDTO {
    @JsonProperty("arriveDate")private LocalDate arriveDate;
    @JsonProperty("leaveDate")private LocalDate leaveDate;
}
