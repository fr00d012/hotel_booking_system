package com.example.demo.dtos.transactionDTO.helperTransactionDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Helper DTO for Transaction
 * DTO to get all room numbers who should get booked
 */
@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
public class RoomDTO {

    @JsonProperty("idRoom") private Integer roomId;
}
