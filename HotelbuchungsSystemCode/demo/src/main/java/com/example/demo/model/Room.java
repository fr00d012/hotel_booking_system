package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;


/**
 * Entity for Rooms
 */
@Setter @Getter @NoArgsConstructor @Entity @AllArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "roomNumber")
public class Room {

//-----------------------------------------VARIABLES--------------------------------------------------------------------
    @Id
    @Column(name="RoomNumber")
    @NotNull(message = "Bitte tragen Sie eine Raumnummer ein.")
    private Integer roomNumber;

    @Column(name="RoomPrice", nullable = false)
    @NotNull(message = "Bitte tragen Sie den Preis pro Nacht ein.")
    private float roomPrice;

    @Column(name = "NumberOfBeds", nullable = false)
    @NotNull(message = "Bitte tragen Sie die Anzahl der Betten ein.")
    private Integer numberOfBed;

    @Column(name="SeaView", nullable = false)
    @NotNull(message = "Bitte geben Sie an ob das Zimmer Meeresblick hat.")
    private Boolean seaView;

    @Column(name="Balcony", nullable = false)
    @NotNull(message = "Bitte geben Sie an ob das Zimmer einen Balkon hat.")
    private Boolean balcony;

    @Column(name="Roomtype", nullable = false)
    @NotEmpty(message = "Bitte geben Sei den Raumtyp an.")
    private String roomtype;

    @Column(name="Bookable",nullable = false)
    @NotNull(message = "Bitte geben Sie an ob das Zimmer gebucht werden kann.")
    private boolean bookable;

//-------------------------CONNECTIONS----------------------------------------------------------------------------------

    @JsonIgnoreProperties({"rooms","roomsStorno","headcount"})
    @ManyToMany(mappedBy = "rooms")
    private Set<Transaction>transaction=new HashSet<>();

    @JsonIgnoreProperties("roomsStorno")
    @ManyToMany(mappedBy = "roomsStorno")
    private Set<StornoTransaction>stornoTransactions=new HashSet<>();

//-----------------------CONSTRUCTOR------------------------------------------------------------------------------------


    public Room(Integer roomNumber, float roomPrice, Integer numberOfBed, Boolean seaView, Boolean balcony, String roomtype, boolean bookable) {
        this.roomNumber = roomNumber;
        this.roomPrice = roomPrice;
        this.numberOfBed = numberOfBed;
        this.seaView = seaView;
        this.balcony = balcony;
        this.roomtype = roomtype;
        this.bookable = bookable;
    }


    public Room(Integer roomNumber, float roomPrice, Integer numberOfBed, Boolean seaView, Boolean balcony, String roomtype) {
        this.roomNumber = roomNumber;
        this.roomPrice = roomPrice;
        this.numberOfBed = numberOfBed;
        this.seaView = seaView;
        this.balcony = balcony;
        this.roomtype = roomtype;
    }

    public void setTransaction(Transaction transaction){this.transaction.add(transaction);}
//-----------------------------------------OVERRIDES--------------------------------------------------------------------
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return roomNumber.equals(room.roomNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roomNumber);
    }
}
