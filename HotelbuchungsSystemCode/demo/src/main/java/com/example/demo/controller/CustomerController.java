package com.example.demo.controller;


import com.example.demo.dtos.customerDTO.CustomerDTO;
import com.example.demo.exceptions.ExceptionStrings;
import com.example.demo.model.Customer;
import com.example.demo.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * This Class is for the Customer Controller
 */
@RestController
public class CustomerController {

    @Autowired
    CustomerService customerService;
//-------------------------GET CONTROLLER-----------------------------------

    /**
     * Get Controller to get all Customer
     *
     * @return ResponseEntity with statuesque
     */
    @CrossOrigin
    @GetMapping("/customer")
    public ResponseEntity<?> getAllCustomer() {
        List<Customer> customers = customerService.getAllCustomers();
        if (customers.size() > 0) {
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Es gibt derzeit keine Gäste in der Datenbank", HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Get Controller to get all Customer without any connections
     *
     * @return ResponseEntity with status message
     */
    @CrossOrigin
    @GetMapping("/customer/allCustomer/noTransaciton")
    public ResponseEntity<?> getAllCustomerWithoutTransactionData() {
        List<CustomerDTO> customerDTOS = customerService.getAllCustomersWithoutTransactions();
        if (customerDTOS.size() > 0) {
            return new ResponseEntity<>(customerDTOS, HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Es gibt derzeit keine Gäste in der Datenbank", HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Get Controller to get one Customer per Id
     *
     * @param userId id from the wanted id
     * @return ResponseEntity with statuesque
     */
    @CrossOrigin
    @GetMapping("/customer/{userId}")
    public ResponseEntity<?> getOneCustomer(@PathVariable Integer userId) {
        try {
            if (customerService.getOneCustomerByUserId(userId) == null) {
                throw new ExceptionStrings("Diese Kunden Id ist nicht vergeben.");
            }
            return new ResponseEntity<>(customerService.getOneCustomerByUserId(userId), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }

    /**
     * Get Controller to search for customers by lastname
     *
     * @param lastname Last name of the wanted customer
     * @return list with founded customers
     */
    @CrossOrigin
    @GetMapping("/customer/search/{lastname}")
    public ResponseEntity<?> getCustomerPerLastname(@PathVariable String lastname) {
        List<Customer> customers = customerService.getCustomerByLastname(lastname);
        if (customers.size() <= 0) {
            return new ResponseEntity<>("Es gibt derzeit keine Gäste mit diesem Nachnamen " + lastname, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(customers, HttpStatus.OK);
        }
    }

    /**
     * Controller to get customer per first and lastname
     *
     * @param firstname Firstname of the wanted customer
     * @param lastname  Lastname of the wanted customer
     * @return customer list with all find customers
     */
    @CrossOrigin
    @GetMapping("/customer/searchfullName/{firstname}/{lastname}")
    public ResponseEntity<?> getCustomerByFullName(@PathVariable String firstname, @PathVariable String lastname) {
        List<Customer> customers = customerService.getCustomerByFullName(firstname, lastname);
        if (customers.size() <= 0) {
            return new ResponseEntity<>("Es gibt derzeit keine Gäste mit diesem Namen " + firstname + " " + lastname, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(customers, HttpStatus.OK);
        }
    }

//-----------------------POST CONTROLLER--------------------------------------------------

    /**
     * Post one Customer in the Database
     *
     * @param customer (Firstname/Lastname/Country/Streetname/ZipCoide/City/Email/BirthDate/Phonenumber)
     */
    @CrossOrigin
    @PostMapping("/add/customer")
    public ResponseEntity<?> addOneCustomer(@Valid @RequestBody Customer customer, BindingResult result) {
        if (result.hasErrors()) {
            List<String> errorMessage = new ArrayList<>();
            List<ObjectError> objectErrors = result.getAllErrors();
            for (ObjectError object : objectErrors) {
                errorMessage.add(object.getDefaultMessage());
            }
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        } else {
            String status = customerService.postOneCustomer(customer);
            if (status.equals("Der Gast wurde hinzugefügt")) {
                return new ResponseEntity<>(status, HttpStatus.OK);
            } else return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
        }


    }

//-----------------------------PUT CONTROLLER----------------------------------------------

    /**
     * Update one Customer per Id
     *
     * @param customer (Firstname/Lastname/Country/Streetname/StreetNumber/ZipCoide/City/Email/BirthDate/Phonenumber)
     * @param userId   customer id
     */
    @CrossOrigin
    @PutMapping("/customer/{userId}")
    public ResponseEntity<?> updateOneCustomerPerId(@Valid @RequestBody Customer customer, BindingResult result, @PathVariable Integer userId) {
        if (result.hasErrors()) {
            List<String> errorMessage = new ArrayList<>();
            List<ObjectError> objectErrors = result.getAllErrors();
            for (ObjectError object : objectErrors) {
                errorMessage.add(object.getDefaultMessage());
            }
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        } else {
            String status = customerService.updateCustomer(customer, userId);
            if (status.equals("Der Gast wurde akualisiert.")) {
                return new ResponseEntity<>(status, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
            }
        }

    }

//----------------------------------DELETE CONTROLLER--------------------------------------

    /**
     * Controller to delete Customer if he has no connection to the transactions
     *
     * @param userId guest/customer id who should get delete
     * @return String with the status
     */
    @CrossOrigin
    @DeleteMapping("/customer/{userId}")
    public ResponseEntity<?> deleteOneCustomerPerId(@Valid @PathVariable Integer userId) {
        String status = customerService.deleteOneCustomer(userId);
        if (status.equals("Gast wurde gelöscht.")) {
            return new ResponseEntity<>(status, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
        }
    }

}
