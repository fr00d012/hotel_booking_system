package com.example.demo.dtos.billDTO.helperBillDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Helper DTO for the Bill (Customer)
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BillCustomerDTO {
    @JsonProperty("firstname")private String firstname;
    @JsonProperty("lastname")private String lastname;
    @JsonProperty("email")private String eMail;

}
