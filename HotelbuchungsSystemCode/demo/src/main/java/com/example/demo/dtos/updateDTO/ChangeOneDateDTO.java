package com.example.demo.dtos.updateDTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * DTO to get one Date
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ChangeOneDateDTO {

    @JsonFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "Bitte tragen Sie ein datum ein.")
    @JsonProperty("localDate")private LocalDate localDate;
}
