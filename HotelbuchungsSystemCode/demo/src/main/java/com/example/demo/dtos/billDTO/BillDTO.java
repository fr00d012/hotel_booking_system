package com.example.demo.dtos.billDTO;

import com.example.demo.dtos.billDTO.helperBillDTO.BillCustomerDTO;
import com.example.demo.dtos.billDTO.helperBillDTO.BillRoomDTO;
import com.example.demo.dtos.billDTO.helperBillDTO.BillTransactionDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * DTO for create a Bill with different ObjectDTO´s
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BillDTO {

    @JsonProperty("transactionData") private BillTransactionDTO billTransactionDTO;
    @JsonProperty("customerData") private BillCustomerDTO billCustomerDTO;
    @JsonProperty("roomData") private List<BillRoomDTO> billRoomDTO;

    @JsonProperty("headcountSize") private Integer headcountsize;
    @JsonProperty("Hotelname") private String hotelname="CODERS.BAY SoftwareHotel";
    @JsonProperty("HotelAdresse") private String adress="gasometerstraße 2 ";
    @JsonProperty("HotelTelefonnummer") private String telephonenumber="+43/6607459856";
    @JsonProperty("HotelEmail")private String eMail="codersbay.softwarehotel@gmx.at";
}
