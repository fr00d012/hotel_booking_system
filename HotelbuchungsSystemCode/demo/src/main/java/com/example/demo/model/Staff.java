package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity for Staff
 */
@Setter @Getter @NoArgsConstructor @Entity @AllArgsConstructor
public class Staff {

//-----------------------------------------VARIABLES--------------------------------------------------------------------
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer staffID;

    @Column(name="FirstName", nullable = false)
    @NotEmpty(message = "Bitte geben Sie einen Vornamen ein.")
    @Size(min=2, max=100, message = "Name muss zwischen 2 und 100 Zeichen sein.")
    private String firstName;

    @Column(name="LastName", nullable = false)
    @NotEmpty(message = "Bitte geben Sie einen Nachnamen ein.")
    @Size(min=2,max=100, message = "Name muss zwischen 2 und 100 Zeichen sein.")
    private String lastName;


    @Column(name="EMail", nullable = false, unique = true)
    @NotEmpty(message = "Bitte geben Sie die E-Mail ein.")
    @Email(message = "E-mail ist ungültig")
    private String eMail;


    @Column(name="adminstatus",nullable = false)
    @NotNull(message = "Bitte geben Sie an ob der Mitarbeiter Adminrechte hat oder nicht.")
    private boolean adminstatus;

    @Column(name="Password", nullable = false)
    @NotEmpty(message = "Bitte legen Sie ein passwort fest.")
    private String password;

    @Column(name="SaveWord", nullable = false)
    @NotEmpty(message = "Bitte legen Sie ein Sicherheitswort fest.")
    private String saveWord;

//-----------------------------------------CONSTRUCTOR------------------------------------------------------------------


    public Staff(String firstName, String lastName, String eMail, boolean adminstatus, String password, String saveWord) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.eMail = eMail;
        this.adminstatus = adminstatus;
        this.password = password;
        this.saveWord = saveWord;
    }
}
