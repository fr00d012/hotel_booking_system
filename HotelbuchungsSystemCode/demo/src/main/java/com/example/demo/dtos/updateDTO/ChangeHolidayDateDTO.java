package com.example.demo.dtos.updateDTO;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class ChangeHolidayDateDTO {


   //@NotNull(message = "Bitte füllen Sie alle Felder aus.")
    @JsonProperty("transactionId")private Integer transactionId;

  //  @NotNull(message = "Bitte füllen Sie alle Felder aus.")
    @JsonProperty("ArriveDate")private LocalDate arriveDate;

   // @NotNull(message = "Bitte füllen Sie alle Felder aus.")
    @JsonProperty("LeaveDate")private LocalDate leaveDate;
}
