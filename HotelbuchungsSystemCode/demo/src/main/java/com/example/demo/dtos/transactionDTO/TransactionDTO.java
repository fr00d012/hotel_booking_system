package com.example.demo.dtos.transactionDTO;

import com.example.demo.dtos.transactionDTO.helperTransactionDTO.HeadcountDTO;
import com.example.demo.dtos.transactionDTO.helperTransactionDTO.RoomDTO;
import com.example.demo.model.Transaction;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.util.List;
import java.util.Set;

/**
 * DTO to create a Transaction
 */
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class TransactionDTO {

    @JsonProperty("transaction") private Transaction transaction;

    @JsonProperty("headcount")private Set<HeadcountDTO> headcount;

    @JsonProperty("Room")private List<RoomDTO> roomId;


}
