package com.example.demo.enumclasses;

/**
 * Enum Class for payment
 */
public enum Payment {
    CASH,
    CREDITCARD,
    BANKCARD,
    PAYPAL;
}
