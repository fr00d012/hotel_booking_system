package com.example.demo.dtos.roomDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

/**
 * DTO to search for free rooms during a duration and by category
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RoomCategoryDTO{

    @JsonProperty("arriveDate")private LocalDate arriveDate;
    @JsonProperty("leaveDate")private LocalDate leaveDate;
    @JsonProperty("RoomType") private String roomType;
}
