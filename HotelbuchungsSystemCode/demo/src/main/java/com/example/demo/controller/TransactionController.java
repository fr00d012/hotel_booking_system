package com.example.demo.controller;

import com.example.demo.dtos.billDTO.BillDTO;
import com.example.demo.dtos.transactionDTO.PrePriceTransactionDTO;
import com.example.demo.dtos.transactionDTO.TransactionChangeRoomDTO;
import com.example.demo.dtos.transactionDTO.TransactionDTOsAsGuest;
import com.example.demo.dtos.transactionDTO.TransactionDTOsWithId;
import com.example.demo.dtos.transactionDTO.helperTransactionDTO.RoomDTO;
import com.example.demo.dtos.updateDTO.ChangeHolidayDateDTO;
import com.example.demo.dtos.updateDTO.ChangeOneDateDTO;
import com.example.demo.model.Transaction;
import com.example.demo.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller Class Transaction
 */
@RestController
public class TransactionController {

    @Autowired
    TransactionService transactionService;

//---------------------------------GET CONTROLLER------------------------------

    /**
     * Controller for all transactions
     *
     * @return all Transaction OR String errorMessage
     */
    @CrossOrigin
    @GetMapping("/transaction")
    public ResponseEntity<?> getallTransaction() {
        List<Transaction> transactions = transactionService.getAllTransaction();
        if (transactions.size() < 1) {
            return new ResponseEntity<>("Es gibt derzeit keine Buchungen.", HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(transactions, HttpStatus.OK);
        }
    }

    /**
     * Controller for get one transaction
     *
     * @param transactionId id of transaction
     * @return Transaction OR String errorMessage
     */
    @CrossOrigin
    @GetMapping("/transaction/{transactionId}")
    public ResponseEntity<?> getOneTransaction(@PathVariable Integer transactionId) {
        Transaction transaction = transactionService.getONeTransactionById(transactionId);
        if (transaction == null) {
            return new ResponseEntity<>("Es gibt keine Buchung mit dieser BuchungsId.", HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(transaction, HttpStatus.OK);
        }
    }


    /**
     * Controller to get all Transaction per Date(arrive Date)
     *
     * @param arriveDate arrive date
     * @return all Transaction for the date OR String errorMessage
     */
    @CrossOrigin
    @GetMapping("/transaction/arriveDate/{arriveDate}")
    public ResponseEntity<?> getAllTransactionByArriveDate(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate arriveDate) {
        List<Transaction> transactions = transactionService.getTransactionPerArriveDate(arriveDate);
        if (transactions.size() < 1) {
            return new ResponseEntity<>("Es gibt keine Buchung mit diesem Anreisedatum.", HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(transactions, HttpStatus.OK);
        }
    }

    /**
     * Get Controller to get the price how much money u will get back by a storno
     *
     * @param transactionId id from the Transaction what u want to storno
     * @return price what the customer get back OR error Message
     */
    @CrossOrigin
    @GetMapping("/transaction/stornoprice/{transactionId}")
    public ResponseEntity<?> getStornoPirce(@PathVariable Integer transactionId) {
        if (transactionService.getONeTransactionById(transactionId) == null) {
            return new ResponseEntity<>("Es gibt keine Buchung mit dieser BuchungsId.", HttpStatus.NOT_FOUND);
        }
        float price = transactionService.getStornoPriceFromATransactionPerId(transactionId);
        if (price >= 0) {
            return new ResponseEntity<>("Wenn Sie stonieren bekommen Sie: " + price + "EUR zurück", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Leider ist ein Fehler aufgetretten bei dem Stonierungs Kostenvoranschlag. Bitte melden Sie sich telefonisch bei uns.", HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Controller to get the Bill for one transaction
     *
     * @param transactionId transaction id where u want to create the bill
     * @return Bill(BillDTO)
     */
    @CrossOrigin
    @GetMapping("/bill/transaction/{transactionId}")
    public ResponseEntity<?> getBill(@PathVariable Integer transactionId) {
        if (transactionService.getONeTransactionById(transactionId) == null) {
            return new ResponseEntity<>("Buchung mit dieser BuchungsID existiert nicht. Bitte überprüfen Sie die Eingabe.", HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(transactionService.getBillPerTransacitonId(transactionId), HttpStatus.OK);
        }
    }

    @CrossOrigin
    @GetMapping("/transaction/prePrice")
    public ResponseEntity<?> getPrePrice(@RequestBody PrePriceTransactionDTO prePriceTransactionDTO) {

        if (!(checkTransactionData(prePriceTransactionDTO.getTransaction()))) {
            return new ResponseEntity<>("Bitte überprüfen Sie die Daten der Buchung.", HttpStatus.BAD_REQUEST);
        }
        if (!(checkTheDate(prePriceTransactionDTO.getTransaction().getArriveDate(), prePriceTransactionDTO.getTransaction().getLeaveDate()))) {
            return new ResponseEntity<>("Bitte überpürfen Sie die Datums Daten.", HttpStatus.BAD_REQUEST);
        }

        if (transactionService.checkIfRoomExisting(prePriceTransactionDTO).size() > 0) {
            List<RoomDTO> notExistingRooms = transactionService.checkIfRoomExisting(prePriceTransactionDTO);
            StringBuilder status = new StringBuilder("Folgende Zimmernummern existieren nicht: ");
            int i = 0;

            for (RoomDTO roomDTO : notExistingRooms) {
                if (i < 1) {
                    status.append(roomDTO.getRoomId());
                } else {
                    status.append(", ").append(roomDTO.getRoomId());
                }
                i++;
            }
            return new ResponseEntity<>(status, HttpStatus.NOT_FOUND);
        }

        float prePrice = transactionService.getPrePriceOfTheBooking(prePriceTransactionDTO);
        String message = "Es würde Sie " + prePrice + "EUR kosten wenn Sie buchen.";
        if (prePrice > 0) {
            return new ResponseEntity<>(message, HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Derzeit nicht möglich bitte wenden Sie sich an unsere Mitarbeiter.", HttpStatus.BAD_REQUEST);
        }

    }

//----------------------POST CONTROLLER-----------------------------------

    /**
     * Add one new Transaction too the Database
     *
     * @param transactionDTOsWithId (ArriveDate/LeaveDate/Payment/FinalPirce/customerId/headcounter)
     * @return String with status message
     */
    @CrossOrigin
    @PostMapping("/add/transaction")
    public ResponseEntity<?> addNewTransaction(@Valid @RequestBody TransactionDTOsWithId transactionDTOsWithId) {

        if (!(checkTransactionData(transactionDTOsWithId.getTransaction()))) {
            return new ResponseEntity<>("Bitte füllen Sie alle erforderlichen Felder aus.", HttpStatus.BAD_REQUEST);
        }
        if (!(checkTheDate(transactionDTOsWithId.getTransaction().getArriveDate(), transactionDTOsWithId.getTransaction().getLeaveDate()))) {
            return new ResponseEntity<>("Bitte überprüfen Sie die eingegeben Daten.", HttpStatus.BAD_REQUEST);
        }

        String status = transactionService.createNewTransaction(transactionDTOsWithId);
        if (status.equals("Buchung war Erfolgreich.")) {
            return new ResponseEntity<>("Buchung war erfolgreich!!", HttpStatus.OK);
        } else {
            return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Controller to post on Transaction as Guest
     *
     * @param transactionDTOsAsGuest (Customer Data/Transaction Data/Headcounter Data)
     * @return String if booking is done or not
     */
    @CrossOrigin
    @PostMapping("/add/transaction/guest")
    public ResponseEntity<?> addNewTransactionGuest(@Valid @RequestBody TransactionDTOsAsGuest transactionDTOsAsGuest, BindingResult result) {
        if (result.hasErrors()) {
            List<ObjectError> errorList = result.getAllErrors();
            List<String> errorMessage = new ArrayList<>();
            for (ObjectError error : errorList) {
                errorMessage.add(error.getDefaultMessage());
            }
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }

        if (!(checkTransactionData(transactionDTOsAsGuest.getTransaction()))) {
            return new ResponseEntity<>("Bitte füllen Sie alle erforderlichen Felder aus.", HttpStatus.BAD_REQUEST);
        }
        if (!(checkTheDate(transactionDTOsAsGuest.getTransaction().getArriveDate(), transactionDTOsAsGuest.getTransaction().getLeaveDate()))) {
            return new ResponseEntity<>("Bitte überprüfen Sie die eingegeben Datums Daten.", HttpStatus.BAD_REQUEST);
        }
        try {
            String status = transactionService.createNewTransactionAsGuest(transactionDTOsAsGuest);
            if (status.equals("Buchung war Erfolgreich")) {
                return new ResponseEntity<>(status, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


    /**
     * Controller to create One Stornotransaction (Delete the transaction from the database and add it to the stornoTransactionDatabase)
     *
     * @param transactionId id from the transaction
     * @return BillDTO or String with error message
     */
    @CrossOrigin
    @PostMapping("/add/stornoTransaction/{transactionId}")
    public ResponseEntity<?> addNewStornoTransaction(@PathVariable Integer transactionId) {

        if (transactionService.getONeTransactionById(transactionId) == null) {
            return new ResponseEntity<>("Es gibt keine Buchung mit dieser BuchungsId.", HttpStatus.NOT_FOUND);
        }
        BillDTO billDTO = transactionService.createOneStronoPerTransactionId(transactionId);
        if (billDTO == null) {
            return new ResponseEntity<>("Es konnte leider keine Rechnung erstellt werden", HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>(billDTO, HttpStatus.OK);
        }
    }

//-----------------------------------------------PUT CONTROLLER---------------------------------------------------------

    /**
     * Cotroller to change The arrive and leaveDate from a HolidayTrip
     *
     * @param changeHolidayDateDTO wanted arriveDate/leaveDate LocalDate
     * @return String with status message
     */
    @CrossOrigin
    @PutMapping("/transaciton/holidayDate")
    public ResponseEntity<?> changeTheHolidayDate(@RequestBody ChangeHolidayDateDTO changeHolidayDateDTO) {
        if (!(checkTheDate(changeHolidayDateDTO.getArriveDate(), changeHolidayDateDTO.getLeaveDate()))) {
            return new ResponseEntity<>("Bitte überprüfen Sie die Datum Daten.", HttpStatus.BAD_REQUEST);
        }
        if (transactionService.getONeTransactionById(changeHolidayDateDTO.getTransactionId()) == null) {
            return new ResponseEntity<>("Buchung mit dieser Id existiert nicht.", HttpStatus.BAD_REQUEST);
        }
        String status = transactionService.updateOneTransactionArriveAndLeaveDate(changeHolidayDateDTO);
        if (status.equals("Buchung wurde aktualisiert.")) {
            return new ResponseEntity<>(status, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Put Controller to change rooms in a transaction
     *
     * @param transactionChangeRoomDTO (roomNumber from the deleted rooms/roomNumber from the added rooms/transactionId)
     * @return ResponseEntity with errorMessage
     */
    @CrossOrigin
    @PutMapping("/transaction/changeRoom/")
    public ResponseEntity<?> changeRoomInTransactionPerId(@RequestBody TransactionChangeRoomDTO transactionChangeRoomDTO) {

        if (!(checkChangeRoomDTO(transactionChangeRoomDTO))) {
            return new ResponseEntity<>("Bitte geben Sie an bei welcher buchung Sie eine Änderung vornehmen wollen.", HttpStatus.BAD_REQUEST);
        }

        String status = transactionService.changeRoomsInTransaction(transactionChangeRoomDTO);
        if (status.equals("Änderung der Zimmer erfolgreich.")) {
            return new ResponseEntity<>(status, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Controller to update transaction per id
     *
     * @param transaction   transaction Data
     * @param result        error result if something is null or empty
     * @param transactionId transaciton id from the transaciton who gets the update
     * @return String with statusMessage
     */
    @CrossOrigin
    @PutMapping("/transaction/{transactionId}")
    public ResponseEntity<?> updateTransaction(@Valid @RequestBody Transaction transaction, BindingResult result, @PathVariable Integer transactionId) {
        if (result.hasErrors()) {
            List<ObjectError> errorList = result.getAllErrors();
            List<String> errorMessage = new ArrayList<>();
            for (ObjectError error : errorList) {
                errorMessage.add(error.getDefaultMessage());
            }
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
        if (!(checkTransactionData(transaction))) {
            return new ResponseEntity<>("Bitte füllen Sie alle erforderlichen Felder aus.", HttpStatus.BAD_REQUEST);
        }
        if (!(checkTheDate(transaction.getArriveDate(), transaction.getLeaveDate()))) {
            return new ResponseEntity<>("Bitte überprüfen Sie die eingegeben Daten.", HttpStatus.BAD_REQUEST);
        }

        String status = transactionService.updateOneTransactionById(transaction, transactionId);

        if (status.equals("Buchung wurde geändert")) {
            return new ResponseEntity<>(status, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Controller to update BookingDate in the Transaction
     *
     * @param changeOneDateDTO wanted Date
     * @param transactionId    id from the transaction where u want to change the booking Date
     */
    @CrossOrigin
    @PutMapping("/transaction/LocalDate/{transactionId}")
    public ResponseEntity<?> updateTransactionLocalDate(@Valid @RequestBody ChangeOneDateDTO changeOneDateDTO, BindingResult result, @PathVariable Integer transactionId) {
        if (result.hasErrors()) {
            List<ObjectError> errorList = result.getAllErrors();
            List<String> errorMessage = new ArrayList<>();
            for (ObjectError error : errorList) {
                errorMessage.add(error.getDefaultMessage());
            }
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }


        String status = transactionService.updateOneTransactionByIdLocalDate(changeOneDateDTO, transactionId);
        if (status.equals("BookingDate wurde aktualisiert.")) {
            return new ResponseEntity<>(status, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
        }
    }
//------------------------------DELETE CONTROLLER-----------------------------------------------------------------------

    /**
     * Controller for delete one transaction
     *
     * @param transactionId transaction id
     */
    @CrossOrigin
    @DeleteMapping("/transaction/{transactionId}")
    public void deleteTransaction(@PathVariable Integer transactionId) {
        transactionService.deleteOneTransaction(transactionId);
    }

    //----------------------------------------HELPER METHODS------------------------------------------------------------

    public boolean checkTheDate(LocalDate arriveDate, LocalDate leaveDate) {
        return !arriveDate.isBefore(LocalDate.now()) && !leaveDate.isBefore(LocalDate.now()) && !leaveDate.isBefore(arriveDate);
    }

    public boolean checkTransactionData(Transaction transaction) {
        return transaction.getArriveDate() != null && transaction.getLeaveDate() != null && transaction.getRooms() != null;
    }

    public boolean checkChangeRoomDTO(TransactionChangeRoomDTO transactionChangeRoomDTO) {
        return transactionChangeRoomDTO.getTransactionId() != null;
    }
}
