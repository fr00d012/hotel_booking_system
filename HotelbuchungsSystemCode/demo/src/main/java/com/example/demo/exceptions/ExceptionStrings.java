package com.example.demo.exceptions;

/**
 * Exeption class to create string messages
 */
public class ExceptionStrings extends Exception{
    public ExceptionStrings(String message){super(message);}
}
