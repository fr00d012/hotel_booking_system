package com.example.demo.dtos.transactionDTO.helperTransactionDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

/**
 * Helper DTO from Transaction
 * DTO for guest who will also arrive
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HeadcountDTO {
    @JsonProperty("headcountFirstname") private String firstname;
    @JsonProperty("headcountLastname") private String lastname;
    @JsonProperty("headcountBirthdate") private LocalDate birthdate;

}
