package com.example.demo.controller;

import com.example.demo.dtos.transactionDTO.helperTransactionDTO.HeadcountDTO;
import com.example.demo.model.Headcount;
import com.example.demo.service.HeadcountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller Class Headcounter
 */
@RestController
public class HeadcountController {
    @Autowired
    HeadcountService headcountService;

//-------------------------------------GET METHODS----------------------------------------------------------------------

    /**
     * Get Controller to get all Headcounts
     * @return ResponseEntity with statuesque
     */
    @CrossOrigin
    @GetMapping("/headcount")
    public ResponseEntity<?> getAllHeadcounts() {
        List<Headcount>headcounts=headcountService.getAllHeadcount();
        if(headcounts.size()<1){
            return new ResponseEntity<>("Es gibt derzeit keine Mitreisende Gäste",HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity<>(headcounts,HttpStatus.OK);
        }
    }

    /**
     * Get Controller too get one Headcount/Person per Id
     *
     * @param headcountId Id from headcount/Person
     * @return ResponseEntity with statuesque
     */
    @CrossOrigin
    @GetMapping("/headcount/{headcountId}")
    public ResponseEntity<?> getOneHeadcount(@PathVariable Integer headcountId) {
      Headcount headcount=headcountService.getOneHeadcountPerId(headcountId);
         if(headcount==null){
             return new ResponseEntity<>("Es gibt derzeit keine Mitreisende Gäste mit dieser Id",HttpStatus.NOT_FOUND);
         }else{
             return new ResponseEntity<>(headcount,HttpStatus.OK);
         }
    }

    /**
     * Controller to get one headcount data without transactionDatas
     * @param headcountId id from the headcount (integer)
     * @return responseEntity with the headcount data or errorMessage
     */
    @CrossOrigin
    @GetMapping("/headcount/id/{headcountId}")
    public ResponseEntity<?>getJustOneHeadcount(@PathVariable Integer headcountId){
        HeadcountDTO headcountDTO=headcountService.getOnlyHeadcount(headcountId);
        if(headcountDTO==null){
            return new ResponseEntity<>("Es gibt derzeit keine Mitreisende Gäste mit dieser Id",HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity<>(headcountDTO,HttpStatus.OK);
        }
    }

//------------------------------------------POST METHODS----------------------------------------------------------------

    /**
     * Post Controller to add one Headcount in the Database
     * @param headcount headcountData( String firstname/ String Lastname/String birthdate)
     * @return ResponseEntity with status message
     */
    @CrossOrigin
    @PostMapping("/add/headcount")
    public ResponseEntity<?> addOneHeadcount(@Valid @RequestBody Headcount headcount, BindingResult result) {
        if(result.hasErrors()) {
            List<ObjectError> errorlist = result.getAllErrors();
            List<String> errorMessages = new ArrayList<>();
            for (ObjectError error : errorlist) {
                errorMessages.add(error.getDefaultMessage());
            }
            return new ResponseEntity<>(errorMessages, HttpStatus.BAD_REQUEST);
        }
            try{
                String status=headcountService.postOneHeadcount(headcount);
                return new ResponseEntity<>(status,HttpStatus.OK);
            }catch (Exception e){
                return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
            }
    }

//-------------------------------------------PUT METHODS----------------------------------------------------------------

    /**
     * update Controller to update one Headcount per Id
     * @param headcount new Headcount data's
     * @param headcountId id from the headcount who get the update
     * @return ResponseEntity with statuesque
     */
    @CrossOrigin
    @PutMapping("/headcount/{headcountId}")
    public ResponseEntity<?> updateOneHeadcount(@Valid@RequestBody Headcount headcount,BindingResult result,@PathVariable Integer headcountId) {
        if(result.hasErrors()){
            List<ObjectError>errorList=result.getAllErrors();
            List<String>errorMessage=new ArrayList<>();
            for(ObjectError error:errorList){
                errorMessage.add(error.getDefaultMessage());
            }
            return new ResponseEntity<>(errorMessage,HttpStatus.BAD_REQUEST);
        }
        String status=headcountService.updateOneHeadcountPerId(headcount,headcountId);
        if(status.equals("Gast wurde akutalisiert.")){
            return new ResponseEntity<>(status,HttpStatus.OK);
        }else {
            return new ResponseEntity<>(status,HttpStatus.NOT_FOUND);
        }
    }

//-------------------------------------------DELETE METHODS-------------------------------------------------------------

    /**
     * Delete controller to delete one headcount per id from the database
     * @param headcountId id from the headcount who gets deleted
     * @return ResponseEntity with statuesque
     */
    @CrossOrigin
    @DeleteMapping("/headcount/{headcountId}")
    public ResponseEntity<String> deleteOneHeadcount(@PathVariable Integer headcountId) {
        String status=headcountService.deleteOneHeadcountPerId(headcountId);
        if(status.equals("Gast wurde gelöscht.")) {
            return new ResponseEntity<>(status,HttpStatus.OK);
        }else {
            return new ResponseEntity<>(status,HttpStatus.BAD_REQUEST);
        }
    }
}
