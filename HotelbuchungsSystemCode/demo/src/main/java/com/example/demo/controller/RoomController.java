package com.example.demo.controller;

import com.example.demo.dtos.roomDTO.AllFreeRoomForADurationDTO;
import com.example.demo.dtos.roomDTO.CountFreeRoomForADurationDTO;
import com.example.demo.dtos.roomDTO.FindRoomByRoomTypeDTO;
import com.example.demo.dtos.roomDTO.RoomCategoryDTO;
import com.example.demo.model.Room;
import com.example.demo.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller vor Room class
 */
@RestController
public class RoomController {

    @Autowired
    RoomService roomService;

//----------------------------------------------GET CONTROLLER----------------------------------------------------------

    /**
     * Controller to get all Rooms from the database
     *
     * @return ResposneEntity with all rooms or errorMessage
     */
    @CrossOrigin
    @GetMapping("/room")
    public ResponseEntity<?> getAllRoom() {
        List<Room> rooms = roomService.getAllRooms();
        if (rooms.size() < 1) {
            return new ResponseEntity<>("Es gibt derzeit keine Räume in der Datenbank.", HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(rooms, HttpStatus.OK);
        }
    }

    /**
     * Controller too get one room by roomnumber
     *
     * @param roomNumber number of the room
     * @return room
     */
    @CrossOrigin
    @GetMapping("/room/{roomNumber}")
    public ResponseEntity<?> getRoomById(@PathVariable Integer roomNumber) {
        Room room = roomService.getOneRoomById(roomNumber);
        if (room == null) {
            return new ResponseEntity<>("Diese Raumnummer ist derzeit nicht vergeben", HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(room, HttpStatus.OK);
        }
    }

    /**
     * Methode to get all free rooms for a duration
     *
     * @param allFreeRoomForADurationDTO (LocalDate arriveDate/LocalDate leaveDate)
     * @return list with all free rooms or errorMessage (String)
     */
    @CrossOrigin
    @GetMapping("/room/freeroom")
    public ResponseEntity<?> getAllFreeRoom(@RequestBody AllFreeRoomForADurationDTO allFreeRoomForADurationDTO) {
        List<Room> room = roomService.getAllfreeRooms(allFreeRoomForADurationDTO);
        if (room.size() < 1) {
            return new ResponseEntity<>("Es gibt derzeit keine freien Räume", HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(room, HttpStatus.OK);
        }
    }

    /**
     * Controller to get all free Rooms counted by categorie and attributes
     *
     * @param allFreeRoomForADurationDTO (LocalDate arriveDate/LocalDate leaveDate)
     * @return all freerooms counted or String with status message
     */
    @CrossOrigin
    @GetMapping("/room/count/freeroom")
    public ResponseEntity<?> countAllFreeRooms(@RequestBody AllFreeRoomForADurationDTO allFreeRoomForADurationDTO) {
        List<CountFreeRoomForADurationDTO> countFreeRoomForADurationDTO = roomService.countTheFreeRooms(allFreeRoomForADurationDTO);
        if (countFreeRoomForADurationDTO.size() < 1) {
            return new ResponseEntity<>("Es gibt derzeit keine Freien Zimmer", HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(countFreeRoomForADurationDTO, HttpStatus.OK);
        }
    }

    /**
     * Controller to get all free rooms from a spezial categorie
     *
     * @param roomCategoryDTO wanted room categorie(String)
     * @return List with all free rooms from the wanted categorie or String with status message
     */
    @CrossOrigin
    @GetMapping("/freeRoom/Category")
    public ResponseEntity<?> getFreeRoomByCategory(@RequestBody RoomCategoryDTO roomCategoryDTO) {
        List<Room> freeRooms = roomService.getFreeRoomPerRoomCategory(roomCategoryDTO);
        if (freeRooms.size() < 1) {
            return new ResponseEntity<>("Es gibt derzeit keine freien Zimmer mit dieser Kategorie.", HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(freeRooms, HttpStatus.OK);
        }
    }

    /**
     * Controller to get the free rooms number the duration from one wanted roomtype
     *
     * @param roomCategoryDTO (arriveDate/leaveDate/roomtype)
     * @return list with all free rooms Number from wanted roomtype or String with status message
     */
    @CrossOrigin
    @GetMapping("/freeRoom/RoomNumber/Category")
    public ResponseEntity<?> getFreeRoomNumberByCategory(@RequestBody RoomCategoryDTO roomCategoryDTO) {
        List<Integer> freeRoomNumber = roomService.getRoomNumberForfreeRoomFromRoomType(roomCategoryDTO);
        if (freeRoomNumber.size() < 1) {
            return new ResponseEntity<>("Es gibt derzeit keine freien Zimmer von dieser Kategorie.", HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(freeRoomNumber, HttpStatus.OK);
        }

    }

    /**
     * Controller to get free rooms by wanted category and room types
     *
     * @param findRoomByRoomTypeDTO (arriveDate/leaveDate/roomCategory/balcony/seaview)
     * @return list with rooms how has the wanted categories or String with status message
     */
    @CrossOrigin
    @GetMapping("/freeRoom/Category/RoomType")
    public ResponseEntity<?> getFreeRoomByCategoryAndType(@RequestBody FindRoomByRoomTypeDTO findRoomByRoomTypeDTO) {
        List<Room> wantedRooms = roomService.getWantedFreeRoomByCategorieAndRoomTypes(findRoomByRoomTypeDTO);
        if (wantedRooms.size() < 1) {
            return new ResponseEntity<>("Es gibt derzeit kein freies Zimmer mit den gewünschten zusätzen", HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(wantedRooms, HttpStatus.OK);
        }
    }

//---------------------------------------------POST CONTROLLER----------------------------------------------------------

    /**
     * Controller to add one room to the database
     * only staff with adminstatus true can add a room
     *
     * @param room                  (int RoomNumber/int roomprice per night/int numberOfBed/boolean seaview/boolean balcony/String roomtype)
     * @param checkStaffAdminstatus id from the staff who wants to add the room
     * @return String with status message
     */
    @CrossOrigin
    @PostMapping("/add/room/{checkStaffAdminstatus}")
    public ResponseEntity<?> addOneRoom(@Valid @RequestBody Room room, BindingResult result, @PathVariable Integer checkStaffAdminstatus) {
        if (result.hasErrors()) {
            List<ObjectError> errorList = result.getAllErrors();
            List<String> errorMessage = new ArrayList<>();
            for (ObjectError error : errorList) {
                errorMessage.add(error.getDefaultMessage());
            }
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        } else {
            String status = roomService.postOneRoom(room, checkStaffAdminstatus);
            if (status.equals("Zimmer wurde hinzugefügt")) {
                return new ResponseEntity<>(status, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
            }
        }
    }

//----------------------------------------------PUT CONTROLLER----------------------------------------------------------


    /**
     * Controller to update one room per roomnumber
     * Only staff with adminstatus true can update rooms
     *
     * @param room                  data what u want to update
     * @param roomnumber            the room what u want to update
     * @param checkStaffAdminstatus id from the staff who wants to update the room
     * @return String
     */
    @CrossOrigin
    @PutMapping("/room/{roomnumber}/{checkStaffAdminstatus}")
    public ResponseEntity<?> updateOneRoom(@RequestBody Room room, @PathVariable Integer roomnumber, @PathVariable Integer checkStaffAdminstatus) {
        String status = roomService.updateBookableByRoomId(room, roomnumber, checkStaffAdminstatus);
        try {
            return new ResponseEntity<>(status, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
        }

    }

//-------------------------------------------DELETE CONTROLLER----------------------------------------------------------

    /**
     * Controller too delete one room by roomnumber
     *
     * @param roomnumber number of the room;
     */
    @CrossOrigin
    @DeleteMapping("/room/{roomnumber}/{checkStaffAdminstatus}")
    public ResponseEntity<String> deleteOneRoom(@PathVariable Integer roomnumber, @PathVariable Integer checkStaffAdminstatus) {
        try {
            return new ResponseEntity<>(roomService.deleteOneRoom(roomnumber, checkStaffAdminstatus), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
