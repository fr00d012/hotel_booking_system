package com.example.demo;


import com.example.demo.dummyData.Dummydata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

    @Autowired
    Dummydata dummydata;


    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        dummydata.createCustomerRandom();
        dummydata.createStaffRandom();
        dummydata.createRoomRandom();
        dummydata.creatTransactionRandom();
        dummydata.createRandomRoomForTransaction();
    }
}






