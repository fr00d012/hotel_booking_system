package com.example.demo.repository;

import com.example.demo.model.Transaction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

/**
 * Repository for Transactions
 */
@Repository
public interface TransactionCRUDRepository extends CrudRepository<Transaction,Integer> {
    List<Transaction> findByArriveDate(LocalDate arriveDate);

    List<Transaction> findByRoomsRoomNumber(Integer roomNumber);

    List<Transaction>findByCustomerUserID(Integer userID);
}
