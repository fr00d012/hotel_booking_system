package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity for hedcounter/Guests
 */
@Setter
@Getter
@NoArgsConstructor
@Entity
@AllArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "headcountId")
public class Headcount {

    //-----------------------------------------VARIABLES--------------------------------------------------------------------
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer headcountId;

    @Column(name = "FirstName")
    @NotEmpty(message = "Bitte geben Sie einen Vornamen ein.")
    @Size(min=2, max=100, message = "Name muss zwischen 2 und 100 Zeichen sein.")
    private String firstName;

    @Column(name = "LastName")
    @NotEmpty(message = "Bitte geben Sie einen Nachnamen ein.")
    @Size(min=2,max=100, message = "Name muss zwischen 2 und 100 Zeichen sein.")
    private String lastName;

    @Column(name = "BirthDate")
    @NotNull(message = "Bitte geben Sie ein Geburtsdatum ein.")
    @Past(message = "Geburtsdatum ungültig")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthdate;
    //-----------------------------------------CONNECTIONS------------------------------------------------------------------
    @ManyToMany(mappedBy = "headcount")
    @Setter(AccessLevel.NONE)
    private Set<Transaction> transaction = new HashSet<>();

//-----------------------------------------CONSTRUCTOR------------------------------------------------------------------

    public Headcount(String firstName, String lastName, LocalDate birthdate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthdate = birthdate;
    }


    //-----------------------------------------GETTER/SETTER----------------------------------------------------------------
    public void setTransaction(Transaction transaction) {
        this.transaction.add(transaction);
    }


}

