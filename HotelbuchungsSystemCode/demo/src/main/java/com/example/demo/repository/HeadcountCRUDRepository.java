package com.example.demo.repository;


import com.example.demo.model.Headcount;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


/**
 * Repository for Headcounter class
 */
@Repository
public interface HeadcountCRUDRepository extends CrudRepository<Headcount, Integer> {
}
