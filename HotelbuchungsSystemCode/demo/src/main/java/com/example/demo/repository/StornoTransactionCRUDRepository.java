package com.example.demo.repository;

import com.example.demo.model.StornoTransaction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for StornoTransactions
 */
@Repository
public interface StornoTransactionCRUDRepository extends CrudRepository<StornoTransaction,Integer> {
}
