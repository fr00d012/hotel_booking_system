package com.example.demo.dtos.transactionDTO;


import com.example.demo.model.Customer;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Extends the transaction DTO to create a transaction with no login (full customer Datas)
 */
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class TransactionDTOsAsGuest extends TransactionDTO {
    @JsonProperty("Customer") private Customer customer;
}
