package com.example.demo.service;

import com.example.demo.dtos.adminDTO.AdminChangePasswordDTO;
import com.example.demo.dtos.staffDTO.StaffDTO;
import com.example.demo.model.Staff;
import com.example.demo.repository.StaffCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service Class for Staff
 */
@Service
public class StaffService {

    @Autowired
    StaffCRUDRepository staffCRUDRepository;


//----------------------------------------------------------GET METHODE-------------------------------------------------

    /**
     * Methode too get all Staff Datas
     *
     * @return list with all of the staff datas
     */
    public List<Staff> getAllStaffList() {
        return (List<Staff>) staffCRUDRepository.findAll();
    }

    /**
     * Methode too get Data from one Staff per Id
     *
     * @param staffId Id from Staff
     * @return Staff Data
     */
    public Staff getOneStaffPerId(Integer staffId) {
        if (staffCRUDRepository.existsById(staffId)) {
            return staffCRUDRepository.findById(staffId).get();
        } else {
            return null;
        }
    }

//----------------------------------------------------------POST METHODE------------------------------------------------

    /**
     * Methode to add staff to the Database if adminstatus is true
     *
     * @param staff   staff u want to add
     * @param staffId id from the staff who want to add the college
     * @return String with status
     */
    public String addOneStaffToTheDatabase(Staff staff, Integer staffId) {


        if (!(staffCRUDRepository.existsById(staffId))) {
            return "Die Id von dem Mitarbeiter der die Mitarbeiter Daten bearbeiten will, existiert nicht. Bitte überprüfen Sie die id.";
        }
        Staff checkStaff = staffCRUDRepository.findById(staffId).get();

        if (!(checkStaff.isAdminstatus())) {
            return "Nicht die Berechtigung um einen Mitarbeiter hinzuzufügen";
        }

        if (staffCRUDRepository.findByeMail(staff.getEMail()).isPresent()) {
            Staff staff1 = staffCRUDRepository.findByeMail(staff.getEMail()).get();
            if (!(staff1.getStaffID().equals(staffId))) {
                return "Diese E-mail ist schon vorhanden";
            }
        }

        staffCRUDRepository.save(staff);
        return "Mitarbeiter wurde hinzugefügt.";

    }

//---------------------------------------------------------PUT METHODE--------------------------------------------------

    /**
     * Methode to update One Staff per Id (checks if admin or not)
     *
     * @param newStaff     Staff Data what u want to update
     * @param staffId      Id from the Staff u want to update
     * @param checkStaffId id from the newStaff who want to update the college
     * @return String with status
     */
    public String updateOneStaffPerId(StaffDTO newStaff, Integer staffId, Integer checkStaffId) {

        if (!(staffCRUDRepository.existsById(checkStaffId))) {
            return "Die Id von dem Mitarbeiter der die Mitarbeiter Daten bearbeiten will, existiert nicht. Bitte überprüfen Sie die id.";
        }
        if (!(staffCRUDRepository.existsById(staffId))) {
            return "Der zu bearbeitende Mitarbeiter ist nicht vorhanden. Bitte überprüfen Sie die Eingabe";
        }

        if (!(staffCRUDRepository.findById(checkStaffId).get().isAdminstatus())) {
            return "Nicht die Berechtigung um einen Mitarbeiter zu bearbeiten";
        }

        if (staffCRUDRepository.findByeMail(newStaff.getEMail()).isPresent()) {
            Staff checkStaffeMail = staffCRUDRepository.findByeMail(newStaff.getEMail()).get();
            if (!(staffId.equals(checkStaffeMail.getStaffID()))) {
                return "E Mail ist falsch. Diese Email ist bei einem anderem Mitarbeiter vorhanden";
            }
        }

        Staff staff = staffCRUDRepository.findById(staffId).get();
        updateStaffData(staff, newStaff);
        return "Mitarbeiter wurde akualisiert";
    }

    /**
     * methode to change the password from one staff
     * @param adminChangePasswordDTO (staffId/oldPassword/newPassword)
     * @return String with status message
     */
    public String changeThePassword(AdminChangePasswordDTO adminChangePasswordDTO) {
        if(!(staffCRUDRepository.existsById(adminChangePasswordDTO.getStaffId()))){
            return "Mitarbeiter mit dieser Id existiert nicht.";
        }
        if(!(staffCRUDRepository.findById(adminChangePasswordDTO.getStaffId()).get().getPassword()).equals(adminChangePasswordDTO.getOldPassword())){
            return "Die Passwörter sind nicht die selben. Bitte überprüfen Sie die Eingabe";
        }
        Staff staff=staffCRUDRepository.findById(adminChangePasswordDTO.getStaffId()).get();
        staff.setPassword(adminChangePasswordDTO.getNewPassword());
        staffCRUDRepository.save(staff);
        return "Passwort wurde geändert";
    }


    public String changeThePasswordwithTheSaveWord(AdminChangePasswordDTO adminChangePasswordDTO){
        if(!(staffCRUDRepository.existsById(adminChangePasswordDTO.getStaffId()))){
            return "Mitarbeiter mit dieser Id existiert nicht.";
        }
        if(!(staffCRUDRepository.findById(adminChangePasswordDTO.getStaffId()).get().getSaveWord()).equals(adminChangePasswordDTO.getOldPassword())){
            return "Die Sicherheitswörter sind nicht die selben. Bitte überprüfen Sie die Eingabe";
        }
        Staff staff=staffCRUDRepository.findById(adminChangePasswordDTO.getStaffId()).get();
        staff.setPassword(adminChangePasswordDTO.getNewPassword());
        staffCRUDRepository.save(staff);

        return "Passwort wurde geändert";
    }
//--------------------------------------------------------DELETE METHODE------------------------------------------------

    /**
     * Methode to delete one Staff from the database Staff (if adminstatus is true also delete from the admin database)
     * Only Admins can delete Staff
     *
     * @param staffId               id from the staff who gets deleted
     * @param checkStaffAdminstatus id from the staff who wants to delete
     * @return String with status message
     */
    public String deleteOneSaffPerId(Integer staffId, Integer checkStaffAdminstatus) {

        if (!(staffCRUDRepository.existsById(checkStaffAdminstatus))) {
            return "Der Mitarbeiter der die Mitarbeiter Daten bearbeiten will, existiert nicht. Bitte überprüfen Sie die Eingabe";
        }
        if (!(staffCRUDRepository.existsById(staffId))) {
            return "Der Mitarbeiter den Sie löschen wollen existiert nicht. Bitte überprüfen Sie die Eingabe";
        }
        if (staffId.equals(checkStaffAdminstatus)) {
            return "Sie können sich nicht selber löschen. Bitte ändern Sie die Eingabe";
        }


        if (staffCRUDRepository.findById(checkStaffAdminstatus).get().isAdminstatus()) {
            staffCRUDRepository.deleteById(staffId);
            return "Mitarbeiter wurde gelöscht";
        } else {
            return "Sie haben nicht die Berechtigung Mitarbeiter zu löschen";
        }


    }

//---------------------------------------------------------HELPER METHODE-----------------------------------------------

    /**
     * Methode to update staff from the database (if adminstatus change to true also create admin in admin database)
     *
     * @param staff    staff who will get updated
     * @param newStaff data what get updated
     */
    public void updateStaffData(Staff staff, StaffDTO newStaff) {
        staff.setFirstName(newStaff.getFirstName());
        staff.setLastName(newStaff.getLastName());
        if (!(newStaff.getEMail().isEmpty())) {
            staff.setEMail(newStaff.getEMail());
        }
        staff.setAdminstatus(newStaff.isAdminstatus());
        staffCRUDRepository.save(staff);

    }

}
