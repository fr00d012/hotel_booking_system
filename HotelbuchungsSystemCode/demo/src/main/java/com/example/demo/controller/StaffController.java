package com.example.demo.controller;

import com.example.demo.dtos.adminDTO.AdminChangePasswordDTO;
import com.example.demo.dtos.staffDTO.StaffDTO;
import com.example.demo.exceptions.ExceptionStrings;
import com.example.demo.model.Staff;
import com.example.demo.service.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller Class Staff
 */
@RestController
public class StaffController {

    @Autowired
    StaffService staffService;
//----------------------------------------------------------GET METHODE-------------------------------------------------

    /**
     * Get Controller too get all Staff datas
     *
     * @return staff Datas
     */
    @CrossOrigin
    @GetMapping("/staff")
    public ResponseEntity<?> getAllStaff() {
        List<Staff> staffs = staffService.getAllStaffList();
        if (staffs.size() > 0) {
            return new ResponseEntity<>(staffs, HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Es gibt derzeit keine Mitarbeiter.", HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Get Controller too get Data from one Staff per Id
     *
     * @param staffId Id from Staff
     * @return Staff data
     */
    @CrossOrigin
    @GetMapping("/staff/{staffId}")
    public ResponseEntity<?> getOneStaff(@PathVariable Integer staffId) {
        try {
            if (staffService.getOneStaffPerId(staffId) == null) {
                throw new ExceptionStrings("Mitarbeiter mit dieser Id exsistiert nicht. Bitte überprüfen Sie die Eingabe.");
            }
            return new ResponseEntity<>(staffService.getOneStaffPerId(staffId), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

//----------------------------------------------------------POST METHODE------------------------------------------------

    /**
     * Controller to post one staff to the database (only Admins can make post staff)
     *
     * @param staff                       StaffDate
     * @param checkAdminStatusFromStaffId from the staff who wants to add the college to the database
     * @return String
     */
    @CrossOrigin
    @PostMapping("/add/staff/{checkAdminStatusFromStaffId}")
    public ResponseEntity<?> postOneStaff(@Valid @RequestBody Staff staff, BindingResult result, @PathVariable Integer checkAdminStatusFromStaffId) {
        if (result.hasErrors()) {
            List<String> errorMessages = new ArrayList<>();
            List<ObjectError> errorList = result.getAllErrors();
            for (ObjectError error : errorList) {
                errorMessages.add(error.getDefaultMessage());
            }
            return new ResponseEntity<>(errorMessages, HttpStatus.BAD_REQUEST);
        } else {
            String status = staffService.addOneStaffToTheDatabase(staff, checkAdminStatusFromStaffId);
            if (status.equals("Mitarbeiter wurde akualisiert")) {
                return new ResponseEntity<>(status, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
            }
        }


    }

//---------------------------------------------------------PUT METHODE--------------------------------------------------

    /**
     * Update one Staff per staff id
     *
     * @param staff   (Firstname/Lastname/streetName/streetNumber/zipCode/city/branch/rang/hireDate/svnr/phonenumber/eMail/Adminstatus)
     * @param staffId id from staff
     */
    @CrossOrigin
    @PutMapping("/staff/update/{staffId}/{checkAdminStatusFromStaffId}")
    public ResponseEntity<?> updateStaffInDatabase(@Valid @RequestBody StaffDTO staff, BindingResult result, @PathVariable Integer staffId, @PathVariable Integer checkAdminStatusFromStaffId) {

        if (result.hasErrors()) {
            List<String> errorMessages = new ArrayList<>();
            List<ObjectError> errorList = result.getAllErrors();
            for (ObjectError error : errorList) {
                errorMessages.add(error.getDefaultMessage());
            }
            return new ResponseEntity<>(errorMessages, HttpStatus.BAD_REQUEST);
        } else {
            String status = staffService.updateOneStaffPerId(staff, staffId, checkAdminStatusFromStaffId);

            if (status.equals("Mitarbeiter wurde akualisiert")) {
                return new ResponseEntity<>(status, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
            }
        }
    }

    @CrossOrigin
    @PutMapping("/staff/changePassword")
    public ResponseEntity<?> changeThePasswordFromStaff(@Valid @RequestBody AdminChangePasswordDTO adminChangePasswordDTO, BindingResult result) {
        if (result.hasErrors()) {
            List<String> errorMessages = new ArrayList<>();
            List<ObjectError> errorList = result.getAllErrors();
            for (ObjectError error : errorList) {
                errorMessages.add(error.getDefaultMessage());
            }
            return new ResponseEntity<>(errorMessages, HttpStatus.BAD_REQUEST);
        } else {
            String status = staffService.changeThePassword(adminChangePasswordDTO);

            if (status.equals("Passwort wurde geändert")) {
                return new ResponseEntity<>(status, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
            }
        }
    }

    @CrossOrigin
    @PutMapping("/staff/changePassword/saveWord")
    public ResponseEntity<?> changeThePasswordwithStaffSaveWord(@Valid @RequestBody AdminChangePasswordDTO adminChangePasswordDTO, BindingResult result) {
        if (result.hasErrors()) {
            List<ObjectError> errorList = result.getAllErrors();
            List<String> allMessages = new ArrayList<>();
            for (ObjectError objectError : errorList) {
                allMessages.add(objectError.getDefaultMessage());
            }
            return new ResponseEntity<>(allMessages, HttpStatus.BAD_REQUEST);
        } else {
            String status = staffService.changeThePasswordwithTheSaveWord(adminChangePasswordDTO);
            if (status.equals("Passwort wurde geändert")) {
                return new ResponseEntity<>(status, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
            }
        }
    }

//--------------------------------------------------------DELETE METHODE------------------------------------------------

    /**
     * Delete one Staff per Id
     *
     * @param staffId staff id
     */
    @CrossOrigin
    @DeleteMapping("/staff/{staffId}/{checkAdminStatusFromStaffId}")
    public ResponseEntity<?> deleteStaff(@PathVariable Integer staffId, @PathVariable Integer checkAdminStatusFromStaffId) {
        String status = staffService.deleteOneSaffPerId(staffId, checkAdminStatusFromStaffId);

        if (status.equals("Mitarbeiter wurde gelöscht")) {
            return new ResponseEntity<>("Mitarbeiter mit der Mitarbeiter ID " + staffId + " wurde entfernt", HttpStatus.OK);
        } else {
            return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
        }

    }
}
