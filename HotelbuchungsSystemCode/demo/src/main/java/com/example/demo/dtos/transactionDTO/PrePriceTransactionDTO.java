package com.example.demo.dtos.transactionDTO;


import com.example.demo.dtos.transactionDTO.helperTransactionDTO.RoomDTO;
import com.example.demo.model.Transaction;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class PrePriceTransactionDTO {

    @JsonProperty("transaction")private Transaction transaction;
    @JsonProperty("Room")private List<RoomDTO> roomId;
}
