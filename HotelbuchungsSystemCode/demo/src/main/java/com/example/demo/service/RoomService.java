package com.example.demo.service;

import com.example.demo.dtos.roomDTO.AllFreeRoomForADurationDTO;
import com.example.demo.dtos.roomDTO.CountFreeRoomForADurationDTO;
import com.example.demo.dtos.roomDTO.FindRoomByRoomTypeDTO;
import com.example.demo.dtos.roomDTO.RoomCategoryDTO;
import com.example.demo.model.Room;
import com.example.demo.model.Transaction;
import com.example.demo.repository.RoomCRUDRepository;
import com.example.demo.repository.StaffCRUDRepository;
import com.example.demo.repository.TransactionCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Service for Room class
 */
@Service
public class RoomService {

    @Autowired
    RoomCRUDRepository roomCRUDRepository;

    @Autowired
    StaffCRUDRepository staffCRUDRepository;

    @Autowired
    TransactionCRUDRepository transactionCRUDRepository;


//-----------------------------------GET METHODS------------------------------------------------------------------------

    /**
     * Methode too get all rooms
     *
     * @return all rooms
     */
    public List<Room> getAllRooms() {
        return (List<Room>) roomCRUDRepository.findAll();
    }

    /**
     * Methode to get one room by roomnumber
     *
     * @param roomNumber number of the room (Integer)
     * @return room with roomdata or null if roomnumber not existing
     */
    public Room getOneRoomById(Integer roomNumber) {
        if (!(roomCRUDRepository.existsById(roomNumber))) {
            return null;
        } else {
            return roomCRUDRepository.findById(roomNumber).get();
        }
    }

    /**
     * Methode to get all rooms how are free during the choosen dates
     *
     * @param allFreeRoomForADurationDTO arrivedate,leavedate
     * @return list with all rooms how are free
     */
    public List<Room> getAllfreeRooms(AllFreeRoomForADurationDTO allFreeRoomForADurationDTO) {
        List<Room> rooms = new ArrayList<>();
        List<Room> freeroom = new ArrayList<>();
        LocalDate arriveDate = allFreeRoomForADurationDTO.getArriveDate();
        LocalDate leaveDate = allFreeRoomForADurationDTO.getLeaveDate();

        for (Room room : roomCRUDRepository.findAll()) {
            if (room.isBookable()) {
                rooms.add(room);
            }
        }

        for (Room room : rooms) {
            boolean roomIsFree = true;

            for (Transaction existingTransaction : room.getTransaction()) {
                if ((existingTransaction.getArriveDate().isBefore(leaveDate))
                        && (existingTransaction.getLeaveDate().isAfter(arriveDate))) {
                    roomIsFree = false;
                } else if (arriveDate.isEqual(existingTransaction.getArriveDate())
                        || leaveDate.isEqual(existingTransaction.getLeaveDate())) {
                    roomIsFree = false;
                }
            }

            if (roomIsFree) {
                freeroom.add(new Room(room.getRoomNumber(),
                        room.getRoomPrice(),
                        room.getNumberOfBed(),
                        room.getSeaView(),
                        room.getBalcony(),
                        room.getRoomtype(),
                        room.isBookable()));
            }
        }
        return freeroom;
    }

    /**
     * Methode to check if the room is free by creating a transaction
     *
     * @param allFreeRoomForADurationDTO arrive date and leave date
     * @return list with all free rooms
     */
    public List<Room> checkAllFreeRoomsToCreateTransaction(AllFreeRoomForADurationDTO allFreeRoomForADurationDTO) {
        List<Room> rooms = new ArrayList<>();
        List<Room> freeroom = new ArrayList<>();
        LocalDate arriveDate = allFreeRoomForADurationDTO.getArriveDate();
        LocalDate leaveDate = allFreeRoomForADurationDTO.getLeaveDate();

        for (Room room : roomCRUDRepository.findAll()) {
            if (room.isBookable()) {
                rooms.add(room);
            }
        }
        for (Room room : rooms) {
            boolean roomIsFree = true;
            for (Transaction existingTransaction : room.getTransaction()) {
                if ((existingTransaction.getArriveDate().isBefore(leaveDate))
                        && (existingTransaction.getLeaveDate().isAfter(arriveDate))) {
                    roomIsFree = false;
                } else if (arriveDate.isEqual(existingTransaction.getArriveDate())
                        || leaveDate.isEqual(existingTransaction.getLeaveDate())) {
                    roomIsFree = false;
                }
            }
            if (roomIsFree) {
                freeroom.add(room);
            }
        }
        return freeroom;
    }

    /**
     * Methode to search free room for a duration by roomtype
     *
     * @param roomCategoryDTO (arriveDate/leaveDate/roomtype)
     * @return list with all free rooms from the wanted roomtype
     */
    public List<Room> getFreeRoomPerRoomCategory(RoomCategoryDTO roomCategoryDTO) {
        AllFreeRoomForADurationDTO allFreeRoomForADurationDTO = new AllFreeRoomForADurationDTO(roomCategoryDTO.getArriveDate(), roomCategoryDTO.getLeaveDate());
        List<Room> freeRooms = getAllfreeRooms(allFreeRoomForADurationDTO);
        List<Room> roomtypeList = new ArrayList<>();
        for (Room room : freeRooms) {
            if (room.getRoomtype().equals(roomCategoryDTO.getRoomType())) {
                roomtypeList.add(room);
            }
        }
        return roomtypeList;
    }

    /**
     * Methode to get the free room Numbers from the searched roomtype
     *
     * @param roomCategoryDTO (arriveDate/leaveDate/roomtype)
     * @return list with the free roomnumbers from the wanted roomtype
     */
    public List<Integer> getRoomNumberForfreeRoomFromRoomType(RoomCategoryDTO roomCategoryDTO) {
        List<Room> roomtypeList = getFreeRoomPerRoomCategory(roomCategoryDTO);
        List<Integer> roomnumber = new ArrayList<>();
        for (Room room : roomtypeList) {
            Integer checkroomNumber = room.getRoomNumber();
            roomnumber.add(checkroomNumber);
        }
        return roomnumber;
    }

    /**
     * Methode to get free rooms filtered by room category and room types
     *
     * @param findRoomByRoomTypeDTO (arriveDate/LeaveDate/Roomcategory/balcony/seaview)
     * @return list with rooms from the wanted rooms
     */
    public List<Room> getWantedFreeRoomByCategorieAndRoomTypes(FindRoomByRoomTypeDTO findRoomByRoomTypeDTO) {

        List<Room> freeRooms = getAllfreeRooms(findRoomByRoomTypeDTO);

        boolean seaview = findRoomByRoomTypeDTO.isSeaview();
        boolean balcony = findRoomByRoomTypeDTO.isBalcony();
        String roomtype = findRoomByRoomTypeDTO.getRoomtype();

        List<Room> wantedRooms;


        if (roomtype.length() == 0) {
            wantedRooms = checkTheFreeRooms(seaview, balcony, freeRooms);
        } else {
            RoomCategoryDTO roomCategoryDTO = new RoomCategoryDTO(findRoomByRoomTypeDTO.getArriveDate(), findRoomByRoomTypeDTO.getLeaveDate(), roomtype);
            freeRooms = getFreeRoomPerRoomCategory(roomCategoryDTO);
            wantedRooms = checkTheFreeRooms(seaview, balcony, freeRooms);
        }
        return wantedRooms;
    }


//-------------------------------------POST METHODS---------------------------------------------------------------------

    /**
     * Methode to add one new room to the Database
     * Only admin can add a room (adminstatus =true)
     *
     * @param room    Data of the room
     * @param staffId id from the staff who wants to add the room
     * @return String with status/error message
     */
    public String postOneRoom(Room room, Integer staffId) {
        if (!(staffCRUDRepository.existsById(staffId))) {
            return "Diese Mitarbeiter Id ist nicht vergeben";
        }
        if (!(staffCRUDRepository.findById(staffId).get().isAdminstatus())) {
            return "Sie haben nicht die Berechtigung Zimmer zu erstellen";
        }
        if (roomCRUDRepository.findByroomNumber(room.getRoomNumber()).isPresent()) {
            return "Diese Zimmernummer gibt es bereits bitte eine andere nehmen";
        }
        roomCRUDRepository.save(room);
        return "Zimmer wurde hinzugefügt";
    }

//------------------------------------UPDATE METHODS--------------------------------------------------------------------

    /**
     * Methode to update a room
     *
     * @param newroom    new room datas
     * @param roomnumber roomnumber from the room who gets updated
     * @param staffId    id from the staff who wants to update the room
     * @return String with message
     */
    public String updateBookableByRoomId(Room newroom, Integer roomnumber, Integer staffId) {

        int guestcounter = 1;
        int bedcounter = 0;
        boolean bedcounterCheck = true;
        StringBuilder errorMessage = new StringBuilder("Das Zimmer konnte nicht akualisiert werden, weil bei folgenden Buchungen zu wenig Betten vorhanden wären." +
                "\n Bitte bei folgenden Buchungsnummern das betroffene Zimmer austauschen vor der änderung. " +
                "\nBuchungsnummern: ");

        if (!(staffCRUDRepository.existsById(staffId))) {
            return "Diese Mitarbeiter Id ist nicht vergeben. Bitte überprüfen Sie die Eingabe";
        }
        if (!(staffCRUDRepository.findById(staffId).get().isAdminstatus())) {
            return "Sie haben nicht die Berechtigung Zimmer zu bearbeiten";
        }
        if (!(roomCRUDRepository.existsById(roomnumber))) {
            return "Diese Zimmernummer ist nicht vergeben. Bitte überprüfen Sie die Eingabe";
        }
        Room room = roomCRUDRepository.findById(roomnumber).get();

        if (roomCRUDRepository.findByroomNumber(newroom.getRoomNumber()).isPresent()) {
            if (!(room.getRoomNumber().equals(newroom.getRoomNumber()))) {
                return "Zimmernummer ist bereits bei einem anderen Zimmer vergeben. Bitte ändern Sie die Eingabe.";
            }
        } else {
            return "Zimmernummer darf derzeit nicht geändert werden.";
        }
        if (newroom.getNumberOfBed() >= room.getNumberOfBed()) {
            updateRoomAndTransactionFinalPrice(room, newroom, roomnumber);
            return "Zimmer wurde aktualisiert.";
        }
        List<Transaction> allTransactions = transactionCRUDRepository.findByRoomsRoomNumber(roomnumber);
        List<Transaction> futureTransaciton = new ArrayList<>();

        //get only transaction from the future
        for (Transaction transaction : allTransactions) {
            if (!(transaction.getArriveDate().isBefore(LocalDate.now()))) {
                futureTransaciton.add(transaction);
            }
        }

        int beddifferenz = room.getNumberOfBed() - newroom.getNumberOfBed();

        //hohle mir die anzahl an Gästen und die Bettenanzahl von dem alten Zimmer
        for (Transaction transaction : futureTransaciton) {
            guestcounter += transaction.getHeadcount().size();
            for (Room room1 : transaction.getRooms()) {
                bedcounter += room1.getNumberOfBed();
            }
            //erzeuge eine errorMessage mit den transactionId wo das Zimmer nicht ausgetauscht werden konnte weil sonst zu wenige betten vorhanden wären
            if (guestcounter > (bedcounter - beddifferenz)) {
                errorMessage.append(transaction.getTransactionID()).append(" ,");
                bedcounterCheck = false;
            }
        }
        //akuallisert mir das zimmer wenn bei allen Buchungen genug Betten vorhanden sind
        if (bedcounterCheck) {
            updateRoomAndTransactionFinalPrice(room, newroom, roomnumber);
            return "Zimmer wurde aktualisiert.";
        } else {
            return errorMessage.toString();
        }
    }


//---------------------------------------------------------DELETE METHODS-----------------------------------------------


    /**
     * Method to delete a room per roomNumber
     * (Replace the room in other transactions, if there is no room free from the same category the room got not deleted)
     *
     * @param roomnumber roomNumber of the room who gets deleted
     * @param staffId    staffId from the staff who wants to delete the room
     * @return String with status or error message
     */
    public String deleteOneRoom(Integer roomnumber, Integer staffId) {
        if (!(staffCRUDRepository.existsById(staffId))) {
            return "Mitarbeiter mit dieser Id existiert nicht. Bitte überprüfen Sie die Eingabe";
        }
        if (!(staffCRUDRepository.findById(staffId).get().isAdminstatus())) {
            return "Sie haben nicht die Berechtigung Zimmer zu löschen";
        }
        if (!(roomCRUDRepository.existsById(roomnumber))) {
            return "Das Zimmer das Sie löschen wollen, existiert nicht. Bitte überprüfen Sie die Eingabe";
        }

        Room room = roomCRUDRepository.findById(roomnumber).get();
        List<Transaction> transactions = transactionCRUDRepository.findByRoomsRoomNumber(roomnumber);
        Set<Transaction> errorTransaction = new HashSet<>();

        for (Transaction transaction : transactions) {
            Set<Room> rooms = transaction.getRooms();

            for (Room checkRoom : rooms) {
                if (checkRoom.getRoomNumber().equals(roomnumber)) {

                    RoomCategoryDTO roomCategoryDTO = new RoomCategoryDTO(transaction.getArriveDate(), transaction.getLeaveDate(), checkRoom.getRoomtype());
                    List<Room> freeRoom = getFreeRoomPerRoomCategory(roomCategoryDTO);

                    if (freeRoom.size() > 0) {

                        Room newRoom = freeRoom.get(0);
                        transaction.getRooms().remove(room);
                        transaction.getRooms().add(newRoom);
                        newRoom.getTransaction().add(transaction);
                        updateTransactionPrice(transaction);

                    } else {

                        errorTransaction.add(transaction);
                        room.setBookable(false);
                        roomCRUDRepository.save(room);
                    }
                    break;
                }
            }
        }

        if (errorTransaction.size() <= 0) {

            room.setTransaction(null);
            roomCRUDRepository.deleteById(roomnumber);
            return "Zimmer wurde gelöscht";

        } else {

            StringBuilder addMessage = new StringBuilder("Bei folgenden Buchungen konnte das Zimmer nicht ersetzt werden: ");
            for (Transaction transaction : errorTransaction) {
                addMessage.append(transaction.getTransactionID()).append(", ");
            }
            return addMessage.toString();
        }
    }

//---------------------------------------------HELPER METHODEN----------------------------------------------------------

    /**
     * Methode to count all free Rooms by their Category and attributes (balcony/seaview/booth or nothing
     *
     * @param rooms     List with all free Rooms for the choosen time
     * @param roomstype List with all room types
     * @return list with roomtypes counted by attributes
     */
    public List<CountFreeRoomForADurationDTO> countAllFreeRoomPerCategorie(List<Room> rooms, Set<String> roomstype) {

        List<CountFreeRoomForADurationDTO> countFreeRoomForADurationDTOS = new ArrayList<>();
        for (String roomName : roomstype) {
            String bedroomType;
            int countbedroomtype = 0;
            int seaview = 0, balcony = 0, nothing = 0, booth = 0;
            bedroomType = roomName;
            for (Room room : rooms) {
                if (roomName.equals(room.getRoomtype())) {
                    countbedroomtype++;
                    if (room.getBalcony() && (!room.getSeaView())) {
                        balcony++;
                    } else if (room.getSeaView() && (!room.getBalcony())) {
                        seaview++;
                    } else if (room.getBalcony() && room.getSeaView()) {
                        booth++;
                    } else {
                        nothing++;
                    }
                }
            }
            CountFreeRoomForADurationDTO countFreeRoomForADurationDTO = new CountFreeRoomForADurationDTO(bedroomType, countbedroomtype, seaview, balcony, nothing, booth);
            countFreeRoomForADurationDTOS.add(countFreeRoomForADurationDTO);
        }
        return countFreeRoomForADurationDTOS;
    }

    /**
     * Methode to find the free Rooms by the wanted roomtype
     *
     * @param seaview   boolean
     * @param balcony   boolean
     * @param freerooms list with all free rooms for the wanted duration
     * @return list with the wanted roomtypes
     */
    public List<Room> checkTheFreeRooms(boolean seaview, boolean balcony, List<Room> freerooms) {
        List<Room> checkedRooms = new ArrayList<>();

        for (Room room : freerooms) {
            if (room.getBalcony() == balcony && room.getSeaView() == seaview) {
                checkedRooms.add(room);
            }
        }
        return checkedRooms;
    }

    /**
     * Methode to get the duration of the holiday
     *
     * @param arriveDate LocalDate arrive Date
     * @param leaveDate  LocalDate leave Date
     * @return Long with the duration of the holiday
     */
    public Long TheDurationOfBooking(LocalDate arriveDate, LocalDate leaveDate) {
        long durationOfHoliday =arriveDate.until(leaveDate, ChronoUnit.DAYS);
        if(durationOfHoliday==0){
            return durationOfHoliday=1;
        }
        return durationOfHoliday;
    }

    /**
     * Methode to get the final Price for the transaction (checks also if some days are inSaison)
     *
     * @param transaction transaction Datas
     * @return Float end Price for the transaction
     */
    public Float updateTransactionPrice(Transaction transaction) {
        Set<Room> rooms = transaction.getRooms();
        long duration = TheDurationOfBooking(transaction.getArriveDate(), transaction.getLeaveDate());
        int yearOfTheBooking = transaction.getArriveDate().getYear();
        LocalDate saisonStart = LocalDate.of(yearOfTheBooking, 5, 30);
        LocalDate saisonEnd = LocalDate.of(yearOfTheBooking, 9, 1);
        long durationInSaison = 0;
        float pricePerNight = 0.0f;

        for (Room room : rooms) {
            pricePerNight += room.getRoomPrice();
        }
        if (transaction.getArriveDate().isBefore(saisonEnd) || transaction.getLeaveDate().isAfter(saisonStart)) {
            for (LocalDate checkDate = transaction.getArriveDate(); checkDate.isBefore(transaction.getLeaveDate()) || checkDate.isEqual(transaction.getLeaveDate()); checkDate = checkDate.plusDays(1)) {
                if (checkDate.isAfter(saisonStart) && checkDate.isBefore(saisonEnd)) {
                    ++durationInSaison;
                }
            }
        }
        long duratonOffSaison = duration - durationInSaison;
        float priceOffSaison = duratonOffSaison * pricePerNight;
        float priceInSaison = (durationInSaison * pricePerNight) * 1.1f;


        return priceInSaison + priceOffSaison;
    }

    /**
     * Methode to Count the free Rooms for the wanted time by their Roomtype and Categories
     *
     * @param allFreeRoomForADurationDTO (arrive Date/Leave Date)
     * @return List with all Room Categories counted
     */
    public List<CountFreeRoomForADurationDTO> countTheFreeRooms(AllFreeRoomForADurationDTO allFreeRoomForADurationDTO) {
        List<Room> freeRooms = getAllfreeRooms(allFreeRoomForADurationDTO);
        Set<String> freeRoomsType = new HashSet<>();

        for (Room room : freeRooms) {
            freeRoomsType.add(room.getRoomtype());
        }
        return countAllFreeRoomPerCategorie(freeRooms, freeRoomsType);
    }

    /**
     * Methode to update the final price in one transcaiotn when the roomprice per night will change
     *
     * @param room       old room object
     * @param newroom    new room object
     * @param roomnumber roomnumber of the room who gets updated
     */
    public void updateRoomAndTransactionFinalPrice(Room room, Room newroom, Integer roomnumber) {
        float checkPrice = room.getRoomPrice();

        room.setRoomNumber(newroom.getRoomNumber());
        room.setRoomPrice(newroom.getRoomPrice());
        room.setNumberOfBed(newroom.getNumberOfBed());
        room.setSeaView(newroom.getSeaView());
        room.setBalcony(newroom.getBalcony());
        room.setRoomtype(newroom.getRoomtype());
        room.setBookable(newroom.isBookable());
        roomCRUDRepository.save(room);

        //falls sich der Zimmerpreis geändert hat wird er hier gleich aktuallisiert in den Buchungen
        if (!(checkPrice == room.getRoomPrice())) {
            List<Transaction> transactions = transactionCRUDRepository.findByRoomsRoomNumber(roomnumber);
            for (Transaction transaction : transactions) {

                Float finalPrice = updateTransactionPrice(transaction);
                transaction.setFinalPrice(finalPrice);
                transactionCRUDRepository.save(transaction);
            }
        }
    }
}

