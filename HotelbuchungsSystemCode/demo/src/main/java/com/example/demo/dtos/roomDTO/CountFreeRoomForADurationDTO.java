package com.example.demo.dtos.roomDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * DTO for count Rooms by Category and Types
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CountFreeRoomForADurationDTO {
    private String bedRoomType;
    private int countbedroomType;
    private int seaview;
    private int balcony;
    private int nothing;
    private int booth;



}
