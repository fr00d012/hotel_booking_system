package com.example.demo.repository;

import com.example.demo.model.Room;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Repository for Room class
 */
@Repository
public interface RoomCRUDRepository extends CrudRepository<Room, Integer> {
    Optional<Room>findByroomNumber(Integer roomNumber);
}
