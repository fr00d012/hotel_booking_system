package com.example.demo.dtos.roomDTO;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * DTO to find free Room by Category and Types
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FindRoomByRoomTypeDTO extends  AllFreeRoomForADurationDTO{
    @JsonProperty ("RoomType")private String roomtype;
    @JsonProperty("Seaview")private boolean seaview;
    @JsonProperty("Balcony")private boolean balcony;

}
