package com.example.demo.service;

import com.example.demo.dtos.customerDTO.CustomerDTO;
import com.example.demo.model.Customer;
import com.example.demo.model.Transaction;
import com.example.demo.repository.CustomerCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Class for Customer Service
 */
@Service
public class CustomerService {

    @Autowired
    CustomerCRUDRepository customerCRUDRepository;

    @Autowired
    TransactionService transactionService;

//---------------------------------------GET METHODE--------------------------------------------------------------------

    /**
     * Methode to get all Customers in the Database
     *
     * @return customerlist;
     */
    public List<Customer> getAllCustomers() {
        return (List<Customer>) customerCRUDRepository.findAll();
    }

    /**
     * Methode to get all customer datas without transactions
     *
     * @return List with customer
     */
    public List<CustomerDTO> getAllCustomersWithoutTransactions() {
        List<Customer> customers = (List<Customer>) customerCRUDRepository.findAll();
        List<CustomerDTO> customerDTOS = new ArrayList<>();
        for (Customer customer : customers) {
            customerDTOS.add(new CustomerDTO(customer.getUserID(),
                    customer.getFirstName(),
                    customer.getLastName(),
                    customer.getCountry(),
                    customer.getStreetName(),
                    customer.getStreetNumber(),
                    customer.getZipCode(),
                    customer.getCity(),
                    customer.getBirthDate(),
                    customer.getPhoneNumber(),
                    customer.getEMail()));
        }
        return customerDTOS;
    }

    /**
     * Methode to get one Customer by userId
     *
     * @param userId Id from the Customer
     * @return Customer
     */
    public Customer getOneCustomerByUserId(Integer userId) {
        if (customerCRUDRepository.existsById(userId)) {
            return customerCRUDRepository.findById(userId).get();
        } else {
            return null;
        }
    }

    /**
     * Methode to get all Customer by lastname
     *
     * @param lastname Lastname from the wanted Customer
     * @return list of customers
     */
    public List<Customer> getCustomerByLastname(String lastname) {
        return customerCRUDRepository.findByLastName(lastname);
    }

    /**
     * Methode to get all customer by full name
     *
     * @param firstname Firstname from the wanted Customer
     * @param lastname  Lastname from the wanted Customer
     * @return list with all founded customers
     */
    public List<Customer> getCustomerByFullName(String firstname, String lastname) {
        return customerCRUDRepository.findByFirstNameAndLastName(firstname, lastname);
    }

//------------------------------------ADD METHODE-----------------------------------------------------------------------

    /**
     * Add one Customer too the database
     *
     * @param customer (Firstname/Lastname/Country/Streetname/StreetNumber/ZipCoide/City/Email/BirthDate/Phonenumber)
     */
    public String postOneCustomer(Customer customer) {
        String status;
        if (customerCRUDRepository.findByeMail(customer.getEMail()).isPresent()) {
            status = "Diese E-Mail existiert bereits bitte überprüfen Sie ihre Eingabe.";
        } else {
            customerCRUDRepository.save(customer);
            status = "Der Gast wurde hinzugefügt";
        }
        return status;
    }

//--------------------------------UPDATE METHODE------------------------------------------------------------------------

    /**
     * Methode too update one Customer
     *
     * @param newCustomer (Firstname/Lastname/Country/Streetname/StreetNumber/ZipCoide/City/Email/BirthDate/Phonenumber)
     * @param userId      customer id
     * @return String with the status
     */
    public String updateCustomer(Customer newCustomer, Integer userId) {
        String status;

        Optional<Customer> checkCustomer = customerCRUDRepository.findById(userId);
        //checkd if the Customer existing
        if (checkCustomer.isPresent()) {
            Customer oldCustomer = checkCustomer.get();
            //check if the given eMail existing in the Database
            if (customerCRUDRepository.findByeMail(newCustomer.getEMail()).isPresent()) {
                Customer controlCustomer = customerCRUDRepository.findByeMail(newCustomer.getEMail()).get();
                //if the given eMail exist equals the customer ID`s if it is the same costumer
                if (controlCustomer.getUserID().equals(oldCustomer.getUserID())) {
                    safeNewCustomer(newCustomer, oldCustomer);
                    status = "Der Gast wurde akualisiert.";
                    //if it is not the same costumer it will not actually the data
                } else {
                    status = "Diese Email ist schon bei einem anderen Gast vorhanden. Bitte überprüfen Sie Ihre eingabe";
                }
            } else {
                safeNewCustomer(newCustomer, oldCustomer);
                status = "Der Gast wurde akualisiert.";
            }
        } else {
            status = "Ein Gast mit dieser Id existiert nicht. Bitte überprüfen Sie ihre eingabe";
        }
        return status;


    }

//-------------------DELETE METHODE-------------------------------------------------------------------------------------

    /**
     * Methode to delete Customer per Id
     *
     * @param userId id from the customer
     * @return String with the status (deleted or not deleleted by this reason)
     */
    public String deleteOneCustomer(Integer userId) {
        String status;
        Optional<Customer> checkCustomer = customerCRUDRepository.findById(userId);

        if (checkCustomer.isPresent()) {
            Customer customer = checkCustomer.get();
            if (customer.getTransaction().isEmpty()) {
                customerCRUDRepository.deleteById(userId);
                status = "Gast wurde gelöscht.";
            } else {
                StringBuilder transactionIds = new StringBuilder();
                List<Transaction> transactions = transactionService.getAllTransacitonPerCustomerId(userId);
                int i = 0;
                for (Transaction transaction : transactions) {
                    int transactionNumber = transaction.getTransactionID();

                    if (i < 1) {
                        transactionIds.append(transaction.getTransactionID());
                    } else {
                        transactionIds.append(", ").append(transactionNumber);
                    }
                    i++;
                }

                status = "Gast kann nicht gelöscht werden, weil dieser noch andere Buchungen offen hat.\n" +
                        "Folgende Buchungen müssen bearbeitet werden: " + transactionIds;
            }

        } else {
            status = "Es gibt keinen Gast mit dieser Id.";
        }
        return status;
    }

//------------------------------------------HELPER METHODS-------------------------------------------------------------

    /**
     * Methode to update one customer (only a helper Methode for the Main Methode)
     *
     * @param newCustomer the new Customer Datas
     * @param oldCustomer the old Customer from the Database
     */
    public void safeNewCustomer(Customer newCustomer, Customer oldCustomer) {
        oldCustomer.setBirthDate(newCustomer.getBirthDate());
        oldCustomer.setCity(newCustomer.getCity());
        oldCustomer.setCountry(newCustomer.getCountry());
        oldCustomer.setFirstName(newCustomer.getFirstName());
        oldCustomer.setLastName(newCustomer.getLastName());
        if (!(newCustomer.getEMail().isEmpty())) {
            oldCustomer.setEMail(newCustomer.getEMail());
        }
        oldCustomer.setPhoneNumber(newCustomer.getPhoneNumber());
        oldCustomer.setStreetName(newCustomer.getStreetName());
        oldCustomer.setStreetNumber(newCustomer.getStreetNumber());
        oldCustomer.setZipCode(newCustomer.getZipCode());
        customerCRUDRepository.save(oldCustomer);
    }
}
