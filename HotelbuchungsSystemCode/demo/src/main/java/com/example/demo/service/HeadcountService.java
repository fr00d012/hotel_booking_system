package com.example.demo.service;

import com.example.demo.dtos.transactionDTO.helperTransactionDTO.HeadcountDTO;
import com.example.demo.model.Headcount;
import com.example.demo.model.Transaction;
import com.example.demo.repository.HeadcountCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;


/**
 * Service class Headcounter class
 */
@Service
public class HeadcountService {

    @Autowired
    HeadcountCRUDRepository headcountCRUDRepository;
//-----------------------------------------GET METHODS------------------------------------------------------------------

    /**
     * Methode to get all headcounts in a list
     *
     * @return list with all headcounts Data
     */
    public List<Headcount> getAllHeadcount() {
        return (List<Headcount>) headcountCRUDRepository.findAll();

    }

    /**
     * Methode to get one Headcount per headcountId
     *
     * @param headcountId id from the headcount u want
     * @return the wanted headcount or null
     */
    public Headcount getOneHeadcountPerId(Integer headcountId) {
        return headcountCRUDRepository.findById(headcountId).orElse(null);
    }

    /**
     * Methode to get one Headcount without transcation Data
     *
     * @param headcountId id from the headcount who i want
     * @return headcountDTO or null
     */
    public HeadcountDTO getOnlyHeadcount(Integer headcountId) {
        if (!(headcountCRUDRepository.existsById(headcountId))) {
            return null;
        }
        Headcount headcount = headcountCRUDRepository.findById(headcountId).get();
        return new HeadcountDTO(headcount.getFirstName(), headcount.getLastName(), headcount.getBirthdate());
    }


//-----------------------------------------POST METHODS-----------------------------------------------------------------

    /**
     * Methode too add one Headcount too the database
     *
     * @param headcount (Fristname/Lastname/Birthdate)
     */
    public String postOneHeadcount(Headcount headcount) {
        headcountCRUDRepository.save(headcount);
        return "Gast wurde hinzugefügt.";
    }

//-----------------------------------------PUT METHODS------------------------------------------------------------------

    /**
     * Methode to update a headocunt in the database per id
     *
     * @param headcount   new headcount data (String firstname/String lastname/LocalDate birthdate)
     * @param headcountId id from the headcount who gets the update
     * @return string with status message
     */
    public String updateOneHeadcountPerId(Headcount headcount, Integer headcountId) {
        if (!(headcountCRUDRepository.existsById(headcountId))) {
            return "Diese GästeId existiert nicht. Bitte überprüfen Sie die Eingabe.";
        }

        Headcount headcounts = headcountCRUDRepository.findById(headcountId).get();
        headcounts.setLastName(headcount.getLastName());
        headcounts.setFirstName(headcount.getFirstName());
        headcounts.setBirthdate(headcount.getBirthdate());
        headcountCRUDRepository.save(headcounts);
        return "Gast wurde akutalisiert.";
    }

//-----------------------------------------DELETE METHODS---------------------------------------------------------------

    /**
     * Methode to delete one headcount per Id from the database also from the transaction
     *
     * @param headcountId id from the headcount who gets deleted
     * @return string with status message
     */
    public String deleteOneHeadcountPerId(Integer headcountId) {
        if (!(headcountCRUDRepository.existsById(headcountId))) {
            return "Gast mit dieser Id existiert nicht. Bitte überprüfen Sie die Eingabe";
        }
        Headcount headcount = headcountCRUDRepository.findById(headcountId).get();
        Set<Transaction> transactions = headcount.getTransaction();
        for (Transaction transaction : transactions) {
            transaction.getHeadcount().remove(headcount);
        }
        headcountCRUDRepository.deleteById(headcountId);
        return "Gast wurde gelöscht.";
    }

}
