package com.example.demo.model;


import com.fasterxml.jackson.annotation.*;

import lombok.*;


import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity for Customer
 */
@Setter
@Getter
@NoArgsConstructor
@Entity
@AllArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "userID")
public class Customer {

//-----------------------------------------CONNECTIONS------------------------------------------------------------------

    @JsonIgnoreProperties("customer")
    @OneToMany(mappedBy = "customer")
    private Set<Transaction> transaction = new HashSet<>();

    @JsonIgnoreProperties("customerStorno")
    @OneToMany(mappedBy = "customerStorno")
    private Set<StornoTransaction> stornoTransactions = new HashSet<>();

//-----------------------------------------VARIABLES--------------------------------------------------------------------

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userID;


    @Column(name = "FirstName", nullable = false)
    @NotEmpty(message = "Bitte geben Sie einen Vornamen ein.")
    @Size(min=2, max=100, message = "Name muss zwischen 2 und 100 Zeichen sein.")
    private String firstName;

    @Column(name = "LastName", nullable = false)
    @NotEmpty(message = "Bitte geben Sie einen Nachnamen ein.")
    @Size(min=2,max=100, message = "Name muss zwischen 2 und 100 Zeichen sein.")
    private String lastName;

    @Column(name = "Country", nullable = false)
    @NotEmpty(message = "Bitte geben Sie ihr Heimatland an.")
    @Size(min=4,max=200,message = "Ländername muss zwischen 4 und 200 Zeichen sein.")
    private String country;

    @Column(name = "Streetname", nullable = false)
    @NotEmpty(message = "Bitte geben Sie den Straßennamen ein.")
    @Size(min=5,message = "Straßenname zu kurz")
    private String streetName;

    @Column(name = "Streetnumber", nullable = false)
    @NotEmpty(message = "Bitte geben Sie die Hausnummer ein.")
    @Size(max=10,message = "Hausnummer zu lang , bitte überprüfen Sie die eingabe")
    private String streetNumber;

    @Column(name = "ZipCode", nullable = false)
    @NotEmpty(message = "Bitte geben Sie die Postleitzahl ein.")
    @Size(min=1, max=10, message = "Postleitzahl muss zwischen 1 und 10 Zeichen sein.")
    private String zipCode;

    @Column(name = "City", nullable = false)
    @NotEmpty(message = "Bitte geben Sie ihre Heimatstadt an.")
    @Size(min=2,max = 100, message = "Stadtname muss zwischen 2 und 100 Zeichen sein.")
    private String city;

    @Column(name = "EMail", nullable = false, unique = true)
    @NotEmpty(message = "Bitte geben Sie die E-Mail ein.")
    @Email(message = "E-mail ist ungültig")
    private String eMail;


    @Column(name = "Birthyear", nullable = false)
    @NotNull(message = "Bitte geben Sie ein Geburtsdatum ein.")
    @Past(message = "Geburtsdatum ungültig")
    private LocalDate birthDate;

    @Column(name = "PhoneNumber", nullable = false)
    @NotEmpty(message = "Bitte geben Sie die Telefonnummer ein.")
    @Size(min=8,max = 15,message = "Telefonnummer muss zwischen 8 und 15 Zeichen lang sein.")
    private String phoneNumber;


    //--------------------------------------------------CONSTRUCTOR-----------------------------------------------------
    public Customer(Set<Transaction> transaction, Integer userID, String firstName, String lastName, String country, String streetName, String streetNumber, String zipCode, String city, String eMail, LocalDate birthDate, String phoneNumber) {
        this.transaction = transaction;
        this.userID = userID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.zipCode = zipCode;
        this.city = city;
        this.eMail = eMail;
        this.birthDate = birthDate;
        this.phoneNumber = phoneNumber;
    }

    public Customer(Integer userID, String firstName, String lastName, String country, String streetName, String streetNumber, String zipCode, String city, String eMail, LocalDate birthDate, String phoneNumber) {
        this.userID = userID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.zipCode = zipCode;
        this.city = city;
        this.eMail = eMail;
        this.birthDate = birthDate;
        this.phoneNumber = phoneNumber;
    }

    public Customer(String firstName, String lastName, String country, String streetName, String streetNumber, String zipCode, String city, String eMail, LocalDate birthDate, String phoneNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.zipCode = zipCode;
        this.city = city;
        this.eMail = eMail;
        this.birthDate = birthDate;
        this.phoneNumber = phoneNumber;
    }

}
